func @_Major.UI.HealthArmorBar.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :13 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :15 :12) // GetComponentInParent<BaseEntityStatsSystem>() (InvocationExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :15 :12) // GetComponentInParent<BaseEntityStatsSystem>().OnHealthArmorEntityChanged (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HandleHealthArmorChange
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :15 :12) // Binary expression on unsupported types GetComponentInParent<BaseEntityStatsSystem>().OnHealthArmorEntityChanged += HandleHealthArmorChange
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HideHealthArmorBars
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :16 :12) // HideHealthArmorBars() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.HealthArmorBar.HandleHealthArmorChange$float.float$(none, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :19 :8) {
^entry (%__healthPct : none, %__armorPct : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :19 :45)
cbde.store %__healthPct, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :19 :45)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :19 :63)
cbde.store %__armorPct, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :19 :63)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :21 :12) // Not a variable of known type: healthBar
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :21 :12) // healthBar.fillAmount (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :21 :35) // Not a variable of known type: _healthPct
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :22 :12) // Not a variable of known type: armorBar
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :22 :12) // armorBar.fillAmount (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :22 :34) // Not a variable of known type: _armorPct
// Entity from another assembly: Invoke
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :23 :19) // nameof(DisplayHealthArmorBars) (InvocationExpression)
%9 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :23 :51)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :23 :12) // Invoke(nameof(DisplayHealthArmorBars), 0) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.HealthArmorBar.DisplayHealthArmorBars$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :26 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :28 :12) // Identifier from another assembly: transform
// Entity from another assembly: Camera
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :28 :29) // Camera.main (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :28 :29) // Camera.main.transform (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :28 :12) // transform.LookAt(Camera.main.transform) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :29 :12) // Identifier from another assembly: transform
%5 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :29 :29)
%6 = constant 180 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :29 :32)
%7 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :29 :37)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :29 :12) // transform.Rotate(0, 180, 0) (InvocationExpression)
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :30 :12) // Not a variable of known type: mainCanvas
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :30 :12) // mainCanvas.gameObject (SimpleMemberAccessExpression)
%11 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :30 :44) // true
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :30 :12) // mainCanvas.gameObject.SetActive(true) (InvocationExpression)
// Entity from another assembly: Invoke
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :31 :19) // nameof(HideHealthArmorBars) (InvocationExpression)
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :31 :48) // Not a variable of known type: displayTime
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :31 :12) // Invoke(nameof(HideHealthArmorBars), displayTime) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.HealthArmorBar.HideHealthArmorBars$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :34 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :36 :12) // Not a variable of known type: mainCanvas
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :36 :12) // mainCanvas.gameObject (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :36 :44) // false
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\HealthArmorBar.cs" :36 :12) // mainCanvas.gameObject.SetActive(false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
