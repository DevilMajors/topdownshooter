func @_Major.GameControllers.ControllerPlayer.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :16 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :18 :30) // GetComponent<Rigidbody>() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :19 :27) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :19 :27) // GlobalDataManager.Instance.playerEntity (SimpleMemberAccessExpression)
// Entity from another assembly: Camera
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :20 :26) // Camera.main (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameControllers.ControllerPlayer.FixedUpdate$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :23 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: handlePlayer
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :25 :12) // handlePlayer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameControllers.ControllerPlayer.handlePlayer$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :28 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :30 :17) // Not a variable of known type: playerEntity
%1 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :30 :17) // playerEntity.IsDead (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :30 :16) // !playerEntity.IsDead (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :30 :16)

^1: // SimpleBlock
// Entity from another assembly: Input
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :32 :48) // "Horizontal" (StringLiteralExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :32 :34) // Input.GetAxis("Horizontal") (InvocationExpression)
// Entity from another assembly: Input
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :33 :46) // "Vertical" (StringLiteralExpression)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :33 :32) // Input.GetAxis("Vertical") (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: handlePlayerMovement
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :35 :16) // handlePlayerMovement() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: handlePlayerRotation
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :36 :16) // handlePlayerRotation() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function handlePlayerMovement(), it contains poisonous unsupported syntaxes

func @_Major.GameControllers.ControllerPlayer.handlePlayerRotation$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :54 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :56 :28) // Not a variable of known type: worldCamera
// Entity from another assembly: Input
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :56 :57) // Input.mousePosition (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :56 :28) // worldCamera.ScreenPointToRay(Input.mousePosition) (InvocationExpression)
// Entity from another assembly: Vector3
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :57 :42) // Vector3.up (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :57 :54) // Vector3.zero (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :57 :32) // new Plane(Vector3.up, Vector3.zero) (ObjectCreationExpression)
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :60 :16) // Not a variable of known type: groundPlane
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :60 :36) // Not a variable of known type: cameraRay
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :60 :51) // Not a variable of known type: rayLength
%12 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :60 :16) // groundPlane.Raycast(cameraRay, out rayLength) (InvocationExpression)
cond_br %12, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :60 :16)

^1: // SimpleBlock
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :62 :38) // Not a variable of known type: cameraRay
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :62 :57) // Not a variable of known type: rayLength
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :62 :38) // cameraRay.GetPoint(rayLength) (InvocationExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :16) // Not a variable of known type: playerRigidbody
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :16) // playerRigidbody.transform (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :61) // Not a variable of known type: pointToLook
%20 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :61) // pointToLook.x (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :76) // Identifier from another assembly: transform
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :76) // transform.position (SimpleMemberAccessExpression)
%23 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :76) // transform.position.y (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :98) // Not a variable of known type: pointToLook
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :98) // pointToLook.z (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :49) // new Vector3(pointToLook.x, transform.position.y, pointToLook.z) (ObjectCreationExpression)
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerPlayer.cs" :63 :16) // playerRigidbody.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z)) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
