func @_Major.EntitySystem.EnemyEntityStatsSystem.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :10 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :12 :20) // GetComponent<Enemy>() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.EnemyEntityStatsSystem.DamageEntity$float$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :17 :8) {
^entry (%__dmgAmount : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :17 :33)
cbde.store %__dmgAmount, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :17 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :19 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :19 :30) // Not a variable of known type: EnemyEntity
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :19 :43) // Not a variable of known type: _dmgAmount
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :19 :12) // base.DamageEntity(EnemyEntity, _dmgAmount) (InvocationExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :20 :12) // Not a variable of known type: enemyEntity
%6 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :20 :12) // enemyEntity.ReceivedDamage (SimpleMemberAccessExpression)
%7 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :20 :41) // true
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.EnemyEntityStatsSystem.GiveHealthEntity$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :23 :8) {
^entry (%__healAmount : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :23 :37)
cbde.store %__healAmount, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :23 :37)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :25 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :25 :34) // Not a variable of known type: EnemyEntity
%3 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :25 :47)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :25 :12) // base.GiveHealthEntity(EnemyEntity, _healAmount) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.EnemyEntityStatsSystem.GiveArmorEntity$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :28 :8) {
^entry (%__armorAmount : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :28 :36)
cbde.store %__armorAmount, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :28 :36)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :30 :12) // base (BaseExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :30 :33) // Not a variable of known type: EnemyEntity
%3 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :30 :46)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :30 :12) // base.GiveArmorEntity(EnemyEntity, _armorAmount) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.EnemyEntityStatsSystem.Die$Major.EntitySystem.BaseEntity$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :33 :8) {
^entry (%__entity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :33 :33)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :33 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :35 :54) // GetComponent<EnemiesStateMachine>() (InvocationExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :36 :16) // Not a variable of known type: enemiesStateMachine
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :36 :39) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :36 :16) // comparison of unknown type: enemiesStateMachine != null
cond_br %5, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :36 :16)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :38 :16) // Not a variable of known type: enemiesStateMachine
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :38 :53) // typeof(DieState) (TypeOfExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :38 :16) // enemiesStateMachine.SwitchToNewState(typeof(DieState)) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.EnemyEntityStatsSystem.DestroyGameObject$UnityEngine.GameObject.float$(none, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :42 :8) {
^entry (%__gameObject : none, %__time : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :42 :38)
cbde.store %__gameObject, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :42 :38)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :42 :62)
cbde.store %__time, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :42 :62)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :44 :15) // Identifier from another assembly: gameObject
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :44 :15) // gameObject.GetComponent<EnemyWeaponController>() (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :44 :67) // null (NullLiteralExpression)
%5 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :44 :15) // comparison of unknown type: gameObject.GetComponent<EnemyWeaponController>() != null
cond_br %5, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :44 :15)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :46 :16) // Identifier from another assembly: gameObject
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :46 :16) // gameObject.GetComponent<EnemyWeaponController>() (InvocationExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :46 :16) // gameObject.GetComponent<EnemyWeaponController>().EnemyWeapon (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :46 :16) // gameObject.GetComponent<EnemyWeaponController>().EnemyWeapon.gameObject (SimpleMemberAccessExpression)
%10 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :46 :98) // false
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :46 :16) // gameObject.GetComponent<EnemyWeaponController>().EnemyWeapon.gameObject.SetActive(false) (InvocationExpression)
br ^2

^2: // SimpleBlock
// Entity from another assembly: Destroy
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :48 :20) // Not a variable of known type: _gameObject
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :48 :33) // Not a variable of known type: _time
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\EnemyEntityStatsSystem.cs" :48 :12) // Destroy(_gameObject, _time) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
