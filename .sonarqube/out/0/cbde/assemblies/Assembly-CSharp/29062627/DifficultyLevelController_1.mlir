func @_Major.GameControllers.DifficultyLevelController.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :21 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :23 :15) // Not a variable of known type: Instance
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :23 :27) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :23 :15) // comparison of unknown type: Instance == null
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :23 :15)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :25 :27) // this (ThisExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function Update(), it contains poisonous unsupported syntaxes

func @_Major.GameControllers.DifficultyLevelController.GetExistingEnemies$$() -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :37 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :39 :19) // FindObjectsOfType<EnemyEntity>() (InvocationExpression)
return %0 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :39 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function ModifyDifficultyLevel(), it contains poisonous unsupported syntaxes

func @_Major.GameControllers.DifficultyLevelController.ChangeEnemyStats$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :55 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :57 :15) // Not a variable of known type: PositiveImpact
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :57 :32) // Not a variable of known type: NegativeImpact
%2 = cmpi "sgt", %0, %1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :57 :15)
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :57 :15)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: IncreaseDifficulty
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :59 :16) // IncreaseDifficulty() (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%4 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :61 :20) // Not a variable of known type: PositiveImpact
%5 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :61 :37) // Not a variable of known type: NegativeImpact
%6 = cmpi "slt", %4, %5 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :61 :20)
cond_br %6, ^4, ^3 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :61 :20)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DecreaseDifficulty
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :63 :16) // DecreaseDifficulty() (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
// Skipping function IncreaseDifficulty(), it contains poisonous unsupported syntaxes

// Skipping function DecreaseDifficulty(), it contains poisonous unsupported syntaxes

func @_Major.GameControllers.DifficultyLevelController.CheckForEnemyChange$Major.EntitySystem.EnemyEntity$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :103 :8) {
^entry (%__enemyEntity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :103 :41)
cbde.store %__enemyEntity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :103 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :105 :15) // Not a variable of known type: _enemyEntity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :105 :15) // _enemyEntity.EntityCurrentArmor (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :105 :49) // Not a variable of known type: _enemyEntity
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :105 :49) // _enemyEntity.EntityMaxArmor (SimpleMemberAccessExpression)
%5 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :105 :15) // comparison of unknown type: _enemyEntity.EntityCurrentArmor > _enemyEntity.EntityMaxArmor
cond_br %5, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :105 :15)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :107 :16) // Not a variable of known type: _enemyEntity
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :107 :16) // _enemyEntity.EntityCurrentArmor (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :107 :50) // Not a variable of known type: _enemyEntity
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :107 :50) // _enemyEntity.EntityMaxArmor (SimpleMemberAccessExpression)
br ^3

^2: // BinaryBranchBlock
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :109 :20) // Not a variable of known type: _enemyEntity
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :109 :20) // _enemyEntity.EntityCurrentHealth (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :109 :55) // Not a variable of known type: _enemyEntity
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :109 :55) // _enemyEntity.EntityMaxHealth (SimpleMemberAccessExpression)
%14 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :109 :20) // comparison of unknown type: _enemyEntity.EntityCurrentHealth > _enemyEntity.EntityMaxHealth
cond_br %14, ^4, ^3 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :109 :20)

^4: // SimpleBlock
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :111 :16) // Not a variable of known type: _enemyEntity
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :111 :16) // _enemyEntity.EntityCurrentHealth (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :111 :51) // Not a variable of known type: _enemyEntity
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :111 :51) // _enemyEntity.EntityMaxHealth (SimpleMemberAccessExpression)
br ^3

^3: // BinaryBranchBlock
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :114 :17) // Not a variable of known type: _enemyEntity
%20 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :114 :17) // _enemyEntity.ReceivedDamage (SimpleMemberAccessExpression)
%21 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :114 :16) // !_enemyEntity.ReceivedDamage (LogicalNotExpression)
cond_br %21, ^5, ^6 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :114 :16)

^5: // SimpleBlock
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :116 :16) // Not a variable of known type: _enemyEntity
%23 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :116 :16) // _enemyEntity.EntityCurrentArmor (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :116 :50) // Not a variable of known type: _enemyEntity
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :116 :50) // _enemyEntity.EntityMaxArmor (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :117 :16) // Not a variable of known type: _enemyEntity
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :117 :16) // _enemyEntity.EntityCurrentHealth (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :117 :51) // Not a variable of known type: _enemyEntity
%29 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :117 :51) // _enemyEntity.EntityMaxHealth (SimpleMemberAccessExpression)
br ^6

^6: // ExitBlock
return

}
func @_Major.GameControllers.DifficultyLevelController.ResetImpact$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :121 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :123 :29)
%1 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\DifficultyLevelController.cs" :124 :29)
br ^1

^1: // ExitBlock
return

}
