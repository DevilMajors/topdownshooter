func @_Major.Timer.ActionTimer.CountTime$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :15 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :17 :17) // Not a variable of known type: isPaused
%1 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :17 :16) // !isPaused (LogicalNotExpression)
cond_br %1, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :17 :16)

^1: // SimpleBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :19 :16) // Not a variable of known type: timer
// Entity from another assembly: Time
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :19 :25) // Time.deltaTime (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :19 :16) // Binary expression on unsupported types timer += Time.deltaTime
br ^2

^2: // ExitBlock
return

}
func @_Major.Timer.ActionTimer.TimerCycle$$() -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :23 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CountTime
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :25 :12) // CountTime() (InvocationExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :26 :16) // Not a variable of known type: timer
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :26 :25) // Not a variable of known type: timeToWait
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :26 :16) // comparison of unknown type: timer >= timeToWait
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :26 :16)

^1: // JumpBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RestartTimer
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :28 :16) // RestartTimer() (InvocationExpression)
%5 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :29 :23) // true
return %5 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :29 :16)

^2: // JumpBlock
%6 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :31 :19) // false
return %6 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :31 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Major.Timer.ActionTimer.TimerExpired$$() -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :34 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CountTime
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :36 :12) // CountTime() (InvocationExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :37 :16) // Not a variable of known type: timer
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :37 :25) // Not a variable of known type: timeToWait
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :37 :16) // comparison of unknown type: timer >= timeToWait
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :37 :16)

^1: // JumpBlock
%4 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :39 :23) // true
return %4 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :39 :16)

^2: // JumpBlock
%5 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :41 :19) // false
return %5 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :41 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Major.Timer.ActionTimer.StartTimer$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :44 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :46 :23) // false
br ^1

^1: // ExitBlock
return

}
func @_Major.Timer.ActionTimer.StopTimer$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :49 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :51 :23) // true
br ^1

^1: // ExitBlock
return

}
func @_Major.Timer.ActionTimer.RestartTimer$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :54 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ActionTimer.cs" :56 :20)
br ^1

^1: // ExitBlock
return

}
