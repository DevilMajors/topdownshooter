func @_SettingsManager.SetMasterVolume$float$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :13 :4) {
^entry (%__value : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :13 :32)
cbde.store %__value, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :13 :32)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :15 :8) // Not a variable of known type: masterVolume
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :15 :30) // "MasterVolume" (StringLiteralExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :15 :46) // Not a variable of known type: _value
%4 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :15 :8) // masterVolume.SetFloat("MasterVolume", _value) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_SettingsManager.SetFullScreen$bool$(i1) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :18 :4) {
^entry (%__fullscreen : i1):
%0 = cbde.alloca i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :18 :30)
cbde.store %__fullscreen, %0 : memref<i1> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :18 :30)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Screen
%1 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :20 :8) // Screen.fullScreen (SimpleMemberAccessExpression)
%2 = cbde.load %0 : memref<i1> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :20 :28)
br ^1

^1: // ExitBlock
return

}
func @_SettingsManager.SetQuality$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :23 :4) {
^entry (%__qualityIndex : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :23 :27)
cbde.store %__qualityIndex, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :23 :27)
br ^0

^0: // SimpleBlock
// Entity from another assembly: QualitySettings
%1 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :25 :40)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :25 :8) // QualitySettings.SetQualityLevel(_qualityIndex) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_SettingsManager.SetResolution$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :28 :4) {
^entry (%__resolutionIndedx : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :28 :30)
cbde.store %__resolutionIndedx, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :28 :30)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :30 :25) // Not a variable of known type: resolutions
%2 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :30 :37)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :30 :25) // resolutions[_resolutionIndedx] (ElementAccessExpression)
// Entity from another assembly: Screen
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :31 :29) // Not a variable of known type: res
%6 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :31 :29) // res.width (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :31 :40) // Not a variable of known type: res
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :31 :40) // res.height (SimpleMemberAccessExpression)
// Entity from another assembly: Screen
%9 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :31 :52) // Screen.fullScreen (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :31 :8) // Screen.SetResolution(res.width, res.height, Screen.fullScreen) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_SettingsManager.GetResolutions$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :34 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :36 :8) // Not a variable of known type: resolutionDropdown
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :36 :8) // resolutionDropdown.ClearOptions() (InvocationExpression)
// Entity from another assembly: Screen
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :37 :22) // Screen.resolutions (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :38 :8) // Not a variable of known type: resolutionDropdown
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResolutionToString
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :38 :57) // Not a variable of known type: resolutions
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :38 :38) // ResolutionToString(resolutions) (InvocationExpression)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :38 :8) // resolutionDropdown.AddOptions(ResolutionToString(resolutions)) (InvocationExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :39 :8) // Not a variable of known type: resolutionDropdown
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :39 :8) // resolutionDropdown.value (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetCurrentResolutionIndex
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :39 :61) // Not a variable of known type: resolutions
%10 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :39 :35) // GetCurrentResolutionIndex(resolutions) (InvocationExpression)
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :40 :8) // Not a variable of known type: resolutionDropdown
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :40 :8) // resolutionDropdown.RefreshShownValue() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ResolutionToString(none), it contains poisonous unsupported syntaxes

// Skipping function GetCurrentResolutionIndex(none), it contains poisonous unsupported syntaxes

func @_SettingsManager.SetQualityDropdown$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :69 :4) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :71 :8) // Not a variable of known type: qualityDropdown
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :71 :8) // qualityDropdown.value (SimpleMemberAccessExpression)
// Entity from another assembly: QualitySettings
%2 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :71 :32) // QualitySettings.GetQualityLevel() (InvocationExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :72 :8) // Not a variable of known type: qualityDropdown
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :72 :8) // qualityDropdown.RefreshShownValue() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_SettingsManager.SetFullScreenAtStart$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :75 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: QualitySettings
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :77 :8) // QualitySettings.vSyncCount (SimpleMemberAccessExpression)
%1 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :77 :37)
// Entity from another assembly: Application
%2 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :78 :8) // Application.targetFrameRate (SimpleMemberAccessExpression)
%3 = constant 60 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :78 :38)
// Entity from another assembly: Screen
%4 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :79 :8) // Screen.fullScreen (SimpleMemberAccessExpression)
%5 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :79 :28) // true
// Entity from another assembly: Screen
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :80 :8) // Screen.fullScreenMode (SimpleMemberAccessExpression)
// Entity from another assembly: FullScreenMode
%7 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :80 :32) // FullScreenMode.ExclusiveFullScreen (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_SettingsManager.SetFullscreenToggleAtStart$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :83 :4) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: Screen
%0 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :85 :12) // Screen.fullScreen (SimpleMemberAccessExpression)
%1 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :85 :37) // true
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :85 :12) // Screen.fullScreen.Equals(true) (InvocationExpression)
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :85 :12)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :87 :12) // Not a variable of known type: toggle
%4 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :87 :12) // toggle.isOn (SimpleMemberAccessExpression)
%5 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :87 :26) // true
br ^3

^2: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :91 :12) // Not a variable of known type: toggle
%7 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :91 :12) // toggle.isOn (SimpleMemberAccessExpression)
%8 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :91 :26) // false
br ^3

^3: // ExitBlock
return

}
func @_SettingsManager.Start$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :95 :4) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetFullScreenAtStart
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :97 :8) // SetFullScreenAtStart() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetResolutions
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :98 :8) // GetResolutions() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetQualityDropdown
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :99 :8) // SetQualityDropdown() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetFullscreenToggleAtStart
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SettingsManager.cs" :100 :8) // SetFullscreenToggleAtStart() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
