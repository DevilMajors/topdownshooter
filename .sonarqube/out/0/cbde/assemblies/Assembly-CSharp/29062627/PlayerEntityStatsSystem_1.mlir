func @_Major.EntitySystem.PlayerEntityStatsSystem.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :10 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :12 :27) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :12 :27) // GlobalDataManager.Instance.playerEntity (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerEntityStatsSystem.GiveMoney$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :15 :8) {
^entry (%__moneyAmount : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :15 :30)
cbde.store %__moneyAmount, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :15 :30)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :17 :16) // Not a variable of known type: playerEntity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :17 :32) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :17 :16) // comparison of unknown type: playerEntity != null
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :17 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :19 :16) // Not a variable of known type: playerEntity
%5 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :19 :16) // playerEntity.MoneyAmount (SimpleMemberAccessExpression)
%6 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :19 :44)
%7 = addi %5, %6 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :19 :16)
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UIStatsRefresh
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :20 :16) // UIStatsRefresh() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerEntityStatsSystem.DamageEntity$float$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :24 :8) {
^entry (%__dmgAmount : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :24 :33)
cbde.store %__dmgAmount, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :24 :33)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :26 :16) // Not a variable of known type: playerEntity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :26 :32) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :26 :16) // comparison of unknown type: playerEntity != null
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :26 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :28 :16) // base (BaseExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :28 :34) // Not a variable of known type: playerEntity
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :28 :48) // Not a variable of known type: _dmgAmount
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :28 :16) // base.DamageEntity(playerEntity, _dmgAmount) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DifficultyLevelController
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :29 :16) // DifficultyLevelController.Instance (SimpleMemberAccessExpression)
%9 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :29 :16) // DifficultyLevelController.Instance.NegativeImpact (SimpleMemberAccessExpression)
%10 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :29 :69)
%11 = addi %9, %10 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :29 :16)
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UIStatsRefresh
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :30 :16) // UIStatsRefresh() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerEntityStatsSystem.GiveHealthEntity$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :34 :8) {
^entry (%__healAmount : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :34 :37)
cbde.store %__healAmount, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :34 :37)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :36 :16) // Not a variable of known type: playerEntity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :36 :32) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :36 :16) // comparison of unknown type: playerEntity != null
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :36 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :38 :16) // base (BaseExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :38 :38) // Not a variable of known type: playerEntity
%6 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :38 :52)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :38 :16) // base.GiveHealthEntity(playerEntity, _healAmount) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DifficultyLevelController
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :39 :16) // DifficultyLevelController.Instance (SimpleMemberAccessExpression)
%9 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :39 :16) // DifficultyLevelController.Instance.PositiveImpact (SimpleMemberAccessExpression)
%10 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :39 :69)
%11 = addi %9, %10 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :39 :16)
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UIStatsRefresh
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :40 :16) // UIStatsRefresh() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerEntityStatsSystem.GiveArmorEntity$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :44 :8) {
^entry (%__armorAmount : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :44 :36)
cbde.store %__armorAmount, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :44 :36)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :46 :16) // Not a variable of known type: playerEntity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :46 :32) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :46 :16) // comparison of unknown type: playerEntity != null
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :46 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :48 :16) // base (BaseExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :48 :37) // Not a variable of known type: playerEntity
%6 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :48 :51)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :48 :16) // base.GiveArmorEntity(playerEntity, _armorAmount) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UIStatsRefresh
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :49 :16) // UIStatsRefresh() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerEntityStatsSystem.Die$Major.EntitySystem.BaseEntity$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :53 :8) {
^entry (%__entity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :53 :33)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :53 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :55 :12) // Not a variable of known type: _entity
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :55 :12) // _entity.IsDead (SimpleMemberAccessExpression)
%3 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :55 :29) // true
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :56 :12) // Identifier from another assembly: gameObject
%5 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :56 :33) // false
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :56 :12) // gameObject.SetActive(false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: RespawnController
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :57 :12) // RespawnController.Instance (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :57 :53) // Not a variable of known type: _entity
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :57 :62) // Identifier from another assembly: gameObject
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :57 :12) // RespawnController.Instance.RespawnPlayer(_entity, gameObject) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerEntityStatsSystem.UIStatsRefresh$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :60 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :62 :12) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :62 :12) // GlobalDataManager.Instance.uIPlayerSystem (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Player\\PlayerEntityStatsSystem.cs" :62 :12) // GlobalDataManager.Instance.uIPlayerSystem.OnPlayerStatsModification() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
