func @_Major.WeaponSystem.EnemyWeapon.ShootWeapon$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :18 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :20 :15) // Not a variable of known type: bulletsLeft
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :20 :30) // Not a variable of known type: bulletsPerTap
%2 = cmpi "sge", %0, %1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :20 :15)
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :20 :15)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ShootSeries
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :22 :16) // ShootSeries() (InvocationExpression)
br ^2

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: HaveBulletsInMagazine
%4 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :25 :17) // HaveBulletsInMagazine() (InvocationExpression)
%5 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :25 :16) // !HaveBulletsInMagazine() (LogicalNotExpression)
cond_br %5, ^3, ^4 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :25 :16)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ReloadWeapon
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :27 :16) // ReloadWeapon() (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Major.WeaponSystem.EnemyWeapon.WeaponInit$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :31 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :33 :26) // Not a variable of known type: magazineSize
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.EnemyWeapon.ReloadWeapon$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :36 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :38 :26) // Not a variable of known type: magazineSize
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.EnemyWeapon.HaveBulletsInMagazine$$() -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :41 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :43 :19) // Not a variable of known type: bulletsLeft
%1 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :43 :33)
%2 = cmpi "sgt", %0, %1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :43 :19)
return %2 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :43 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Major.WeaponSystem.EnemyWeapon.ShootSeries$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :46 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
%0 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :25)
%1 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :21) // i
cbde.store %0, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :21)
br ^1

^1: // BinaryBranchBlock
%2 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :28)
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :32) // Not a variable of known type: bulletsPerTap
%4 = cmpi "slt", %2, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :28)
cond_br %4, ^2, ^3 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :28)

^2: // SimpleBlock
// Entity from another assembly: Random
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :50 :53) // Not a variable of known type: spreadAngle
%6 = cbde.neg %5 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :50 :52)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :50 :66) // Not a variable of known type: spreadAngle
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :50 :39) // Random.Range(-spreadAngle, spreadAngle) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameObjectPool
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :51 :36) // GameObjectPool.Instance (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :51 :73) // Not a variable of known type: bulletPrefab
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :51 :36) // GameObjectPool.Instance.GetPooledObj(bulletPrefab) (InvocationExpression)
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :52 :16) // Not a variable of known type: bullet
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :52 :16) // bullet.transform (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :52 :16) // bullet.transform.position (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :52 :44) // Not a variable of known type: shootingPoint
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :52 :44) // shootingPoint.position (SimpleMemberAccessExpression)
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :16) // Not a variable of known type: bullet
%20 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :16) // bullet.transform (SimpleMemberAccessExpression)
%21 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :16) // bullet.transform.rotation (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :44) // Not a variable of known type: shootingPoint
%23 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :44) // shootingPoint.rotation (SimpleMemberAccessExpression)
// Entity from another assembly: Quaternion
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :90) // Not a variable of known type: tmpSpreadAngle
// Entity from another assembly: Vector3
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :106) // Vector3.up (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :69) // Quaternion.AngleAxis(tmpSpreadAngle, Vector3.up) (InvocationExpression)
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :53 :44) // Binary expression on unsupported types shootingPoint.rotation * Quaternion.AngleAxis(tmpSpreadAngle, Vector3.up)
%28 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :55 :30) // Not a variable of known type: bullet
%29 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :55 :30) // bullet.GetComponent<Bullet>() (InvocationExpression)
%31 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :56 :16) // Not a variable of known type: bull
%32 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :56 :16) // bull.bulletDamage (SimpleMemberAccessExpression)
%33 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :56 :36) // Not a variable of known type: GunDamage
%34 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :57 :16) // Not a variable of known type: bull
%35 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :57 :16) // bull.bulletSpeed (SimpleMemberAccessExpression)
%36 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :57 :35) // Not a variable of known type: bulletSpeed
%37 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :58 :16) // Not a variable of known type: bull
%38 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :58 :16) // bull.rangeOfLife (SimpleMemberAccessExpression)
%39 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :58 :35) // Not a variable of known type: Range
%40 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :59 :16) // Not a variable of known type: bull
%41 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :59 :16) // bull.shootBy (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :59 :31) // Identifier from another assembly: gameObject
%43 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :59 :31) // gameObject.transform (SimpleMemberAccessExpression)
%44 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :59 :31) // gameObject.transform.parent (SimpleMemberAccessExpression)
%45 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :59 :31) // gameObject.transform.parent.gameObject (SimpleMemberAccessExpression)
%46 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :61 :16) // Not a variable of known type: bullet
%47 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :61 :33) // true
%48 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :61 :16) // bullet.SetActive(true) (InvocationExpression)
%49 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :62 :16) // Not a variable of known type: bulletsLeft
%50 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :62 :16) // Inc/Decrement of field or property bulletsLeft
br ^4

^4: // SimpleBlock
%51 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :47)
%52 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :47)
%53 = addi %51, %52 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :47)
cbde.store %53, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\EnemyWeapon.cs" :48 :47)
br ^1

^3: // ExitBlock
return

}
