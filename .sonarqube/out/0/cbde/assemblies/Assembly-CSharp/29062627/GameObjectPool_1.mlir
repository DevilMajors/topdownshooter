func @_Major.GameControllers.GameObjectPool.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :10 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :12 :16) // Not a variable of known type: Instance
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :12 :28) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :12 :16) // comparison of unknown type: Instance == null
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :12 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :14 :27) // this (ThisExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.GameControllers.GameObjectPool.GetPooledObj$UnityEngine.GameObject$(none) -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :18 :8) {
^entry (%__bullet : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :18 :39)
cbde.store %__bullet, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :18 :39)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :20 :16) // Not a variable of known type: objects
%2 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :20 :16) // objects.Count (SimpleMemberAccessExpression)
%3 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :20 :33)
%4 = cmpi "sle", %2, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :20 :16)
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :20 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: AddGameObject
%5 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :22 :30)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :22 :33) // Not a variable of known type: _bullet
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :22 :16) // AddGameObject(1, _bullet) (InvocationExpression)
br ^2

^2: // JumpBlock
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :24 :19) // Not a variable of known type: objects
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :24 :19) // objects.Dequeue() (InvocationExpression)
return %9 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :24 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Major.GameControllers.GameObjectPool.ReturnToPool$UnityEngine.GameObject$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :27 :8) {
^entry (%__objectToReturn : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :27 :33)
cbde.store %__objectToReturn, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :27 :33)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :29 :12) // Not a variable of known type: _objectToReturn
%2 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :29 :38) // false
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :29 :12) // _objectToReturn.SetActive(false) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :30 :12) // Not a variable of known type: objects
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :30 :28) // Not a variable of known type: _objectToReturn
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :30 :12) // objects.Enqueue(_objectToReturn) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameControllers.GameObjectPool.AddGameObject$int.UnityEngine.GameObject$(i32, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :33 :8) {
^entry (%__number : i32, %__bullet : none):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :33 :34)
cbde.store %__number, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :33 :34)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :33 :47)
cbde.store %__bullet, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :33 :47)
br ^0

^0: // ForInitializerBlock
%2 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :24)
%3 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :20) // i
cbde.store %2, %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :20)
br ^1

^1: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :27)
%5 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :31)
%6 = cmpi "slt", %4, %5 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :27)
cond_br %6, ^2, ^3 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :27)

^2: // SimpleBlock
// Entity from another assembly: Instantiate
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :37 :48) // Not a variable of known type: _bullet
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :37 :36) // Instantiate(_bullet) (InvocationExpression)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :38 :16) // Not a variable of known type: newGameObject
%11 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :38 :40) // false
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :38 :16) // newGameObject.SetActive(false) (InvocationExpression)
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :39 :16) // Not a variable of known type: objects
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :39 :32) // Not a variable of known type: newGameObject
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :39 :16) // objects.Enqueue(newGameObject) (InvocationExpression)
br ^4

^4: // SimpleBlock
%16 = cbde.load %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :40)
%17 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :40)
%18 = addi %16, %17 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :40)
cbde.store %18, %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\GameObjectPool.cs" :35 :40)
br ^1

^3: // ExitBlock
return

}
