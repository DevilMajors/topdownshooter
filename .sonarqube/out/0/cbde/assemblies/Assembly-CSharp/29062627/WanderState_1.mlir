func @_Major.EntitySystem.WanderState.Tick$$() -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :28 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CheckForPlayerEntity
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :30 :36) // CheckForPlayerEntity() (InvocationExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :31 :16) // Not a variable of known type: chaseTarget
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :31 :31) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :31 :16) // comparison of unknown type: chaseTarget != null
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :31 :16)

^1: // JumpBlock
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :33 :16) // Not a variable of known type: enemy
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :33 :32) // Not a variable of known type: chaseTarget
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :33 :16) // enemy.SetTarget(chaseTarget) (InvocationExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :34 :23) // typeof(ChaseState) (TypeOfExpression)
return %8 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :34 :16)

^2: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ReachedTarget
%9 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :37 :16) // ReachedTarget() (InvocationExpression)
cond_br %9, ^3, ^4 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :37 :16)

^3: // SimpleBlock
%10 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :39 :28) // false
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :40 :16) // Not a variable of known type: enemy
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :40 :16) // enemy.animator (SimpleMemberAccessExpression)
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :40 :39) // "isWalking" (StringLiteralExpression)
%14 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :40 :52) // false
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :40 :16) // enemy.animator.SetBool("isWalking", false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetNewDestination
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :41 :16) // SetNewDestination() (InvocationExpression)
br ^4

^4: // BinaryBranchBlock
%17 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :44 :16) // Not a variable of known type: isWalking
%18 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :44 :15) // !isWalking (LogicalNotExpression)
cond_br %18, ^5, ^6 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :44 :15)

^5: // BinaryBranchBlock
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :46 :20) // Not a variable of known type: IdleTimer
%20 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :46 :20) // IdleTimer.TimerCycle() (InvocationExpression)
cond_br %20, ^7, ^8 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :46 :20)

^7: // SimpleBlock
%21 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :48 :32) // true
br ^8

^6: // SimpleBlock
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :54 :16) // Not a variable of known type: enemy
%23 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :54 :16) // enemy.animator (SimpleMemberAccessExpression)
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :54 :39) // "isWalking" (StringLiteralExpression)
%25 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :54 :52) // true
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :54 :16) // enemy.animator.SetBool("isWalking", true) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WanderTowardsDestinationPoint
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :55 :16) // WanderTowardsDestinationPoint() (InvocationExpression)
br ^8

^8: // JumpBlock
%28 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :57 :19) // typeof(WanderState) (TypeOfExpression)
return %28 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :57 :12)

^9: // ExitBlock
cbde.unreachable

}
// Skipping function CheckForPlayerEntity(), it contains poisonous unsupported syntaxes

func @_Major.EntitySystem.WanderState.GetRandomPointToWander$UnityEngine.Vector3.int$(none, i32) -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :77 :8) {
^entry (%__origin : none, %__layermask : i32):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :77 :47)
cbde.store %__origin, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :77 :47)
%1 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :77 :64)
cbde.store %__layermask, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :77 :64)
br ^0

^0: // BinaryBranchBlock
// Entity from another assembly: UnityEngine
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :38) // UnityEngine.Random (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :38) // UnityEngine.Random.insideUnitSphere (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :76) // Not a variable of known type: detectionRange
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :38) // Binary expression on unsupported types UnityEngine.Random.insideUnitSphere * detectionRange
%7 = constant 2 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :93)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :38) // Binary expression on unsupported types UnityEngine.Random.insideUnitSphere * detectionRange * 2
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :97) // Not a variable of known type: _origin
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :80 :38) // Binary expression on unsupported types UnityEngine.Random.insideUnitSphere * detectionRange * 2 + _origin
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :81 :42) // Not a variable of known type: randomDirection
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :81 :42) // randomDirection.x (SimpleMemberAccessExpression)
%14 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :81 :61)
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :81 :64) // Not a variable of known type: randomDirection
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :81 :64) // randomDirection.z (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :81 :30) // new Vector3(randomDirection.x, 0, randomDirection.z) (ObjectCreationExpression)
// Entity from another assembly: NavMesh
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :82 :35) // Not a variable of known type: randomDirection
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :82 :56) // Not a variable of known type: navHit
%20 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :82 :64) // Not a variable of known type: detectionRange
%21 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :82 :80)
%22 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :82 :12) // NavMesh.SamplePosition(randomDirection, out navHit, detectionRange, _layermask) (InvocationExpression)
%23 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :84 :19) // new NavMeshPath() (ObjectCreationExpression)
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :85 :12) // Not a variable of known type: enemy
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :85 :12) // enemy.NavAgent (SimpleMemberAccessExpression)
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :85 :41) // Not a variable of known type: transform
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :85 :41) // transform.position (SimpleMemberAccessExpression)
%28 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :85 :61) // Not a variable of known type: path
%29 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :85 :12) // enemy.NavAgent.CalculatePath(transform.position, path) (InvocationExpression)
%30 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :86 :16) // Not a variable of known type: path
%31 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :86 :16) // path.status (SimpleMemberAccessExpression)
// Entity from another assembly: NavMeshPathStatus
%32 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :86 :31) // NavMeshPathStatus.PathPartial (SimpleMemberAccessExpression)
%33 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :86 :16) // comparison of unknown type: path.status == NavMeshPathStatus.PathPartial
cond_br %33, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :86 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetRandomPointToWander
%34 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :88 :39) // Not a variable of known type: transform
%35 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :88 :39) // transform.position (SimpleMemberAccessExpression)
%36 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :88 :60)
%37 = cbde.neg %36 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :88 :59)
%38 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :88 :16) // GetRandomPointToWander(transform.position, -1) (InvocationExpression)
br ^2

^2: // JumpBlock
%39 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :90 :19) // Not a variable of known type: navHit
%40 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :90 :19) // navHit.position (SimpleMemberAccessExpression)
return %40 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :90 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Major.EntitySystem.WanderState.SetNewDestination$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :93 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetRandomPointToWander
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :95 :54) // Not a variable of known type: transform
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :95 :54) // transform.position (SimpleMemberAccessExpression)
%2 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :95 :75)
%3 = cbde.neg %2 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :95 :74)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :95 :31) // GetRandomPointToWander(transform.position, -1) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.WanderState.WanderTowardsDestinationPoint$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :98 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :100 :12) // Not a variable of known type: enemy
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :100 :12) // enemy.NavAgent (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :100 :42) // Not a variable of known type: destinationPoint
%3 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :100 :12) // enemy.NavAgent.SetDestination(destinationPoint) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetNewDestinationOnFrontBlocked
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :101 :12) // SetNewDestinationOnFrontBlocked() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.WanderState.ReachedTarget$$() -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :104 :8) {
^entry :
br ^0

^0: // JumpBlock
// Entity from another assembly: Vector3
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :106 :59) // Not a variable of known type: transform
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :106 :59) // transform.position (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :106 :79) // Not a variable of known type: destinationPoint
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :106 :42) // Vector3.Distance(transform.position, destinationPoint) (InvocationExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :107 :19) // Not a variable of known type: distanceToDestination
%6 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :107 :44) // 1f (NumericLiteralExpression)
%7 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :107 :19) // comparison of unknown type: distanceToDestination <= 1f
return %7 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\WanderState.cs" :107 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function SetNewDestinationOnFrontBlocked(), it contains poisonous unsupported syntaxes

