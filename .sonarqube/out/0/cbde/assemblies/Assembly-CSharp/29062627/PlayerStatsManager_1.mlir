func @_Major.EntitySystem.PlayerStatsManager.Start$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :15 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :17 :27) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :17 :27) // GlobalDataManager.Instance.playerEntity (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :18 :40) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :18 :40) // GlobalDataManager.Instance.playerEntityStatsUpgrades (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerStatsManager.SubtractMoney$int$(i32) -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :21 :8) {
^entry (%__upgradeCost : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :21 :35)
cbde.store %__upgradeCost, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :21 :35)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :23 :16) // Not a variable of known type: playerEntity
%2 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :23 :16) // playerEntity.MoneyAmount (SimpleMemberAccessExpression)
%3 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :23 :44)
%4 = cmpi "sge", %2, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :23 :16)
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :23 :16)

^1: // JumpBlock
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :25 :16) // Not a variable of known type: playerEntity
%6 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :25 :16) // playerEntity.MoneyAmount (SimpleMemberAccessExpression)
%7 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :25 :44)
%8 = subi %6, %7 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :25 :16)
// No identifier name for binary assignment expression
%9 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :26 :23) // true
return %9 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :26 :16)

^2: // JumpBlock
%10 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :30 :23) // false
return %10 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :30 :16)

^3: // ExitBlock
cbde.unreachable

}
func @_Major.EntitySystem.PlayerStatsManager.SetPlayerStatsToMaxOnLevelEnter$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :34 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :36 :12) // Not a variable of known type: playerEntity
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :36 :12) // playerEntity.EntityCurrentArmor (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :36 :46) // Not a variable of known type: playerEntity
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :36 :46) // playerEntity.EntityMaxArmor (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :37 :12) // Not a variable of known type: playerEntity
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :37 :12) // playerEntity.EntityCurrentHealth (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :37 :47) // Not a variable of known type: playerEntity
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :37 :47) // playerEntity.EntityMaxHealth (SimpleMemberAccessExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerStatsManager.UpgradeHealth$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :40 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :42 :16) // Not a variable of known type: playerEntityStatsUpgrades
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :42 :16) // playerEntityStatsUpgrades.CurrentHealthUpgrade (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :42 :65) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :42 :65) // playerEntityStatsUpgrades.MaxHealthUpgrade (SimpleMemberAccessExpression)
%4 = cmpi "slt", %1, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :42 :16)
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :42 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SubtractMoney
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :44 :34) // Not a variable of known type: upgradeCosts
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :44 :47) // Not a variable of known type: playerEntityStatsUpgrades
%7 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :44 :47) // playerEntityStatsUpgrades.CurrentHealthUpgrade (SimpleMemberAccessExpression)
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :44 :34) // upgradeCosts[playerEntityStatsUpgrades.CurrentHealthUpgrade] (ElementAccessExpression)
%9 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :44 :20) // SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentHealthUpgrade]) (InvocationExpression)
cond_br %9, ^3, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :44 :20)

^3: // SimpleBlock
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :46 :20) // Not a variable of known type: playerEntityStatsUpgrades
%11 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :46 :20) // playerEntityStatsUpgrades.CurrentHealthUpgrade (SimpleMemberAccessExpression)
%12 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :46 :70)
%13 = addi %11, %12 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :46 :20)
// No identifier name for binary assignment expression
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :47 :20) // Not a variable of known type: playerEntity
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :47 :20) // playerEntity.EntityMaxHealth (SimpleMemberAccessExpression)
%16 = constant 25 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :47 :52)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :47 :20) // Binary expression on unsupported types playerEntity.EntityMaxHealth += 25
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InvokeStatsUpgrade
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :48 :20) // InvokeStatsUpgrade() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerStatsManager.UpgradeArmor$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :53 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :55 :16) // Not a variable of known type: playerEntityStatsUpgrades
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :55 :16) // playerEntityStatsUpgrades.CurrentArmorUpgrade (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :55 :64) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :55 :64) // playerEntityStatsUpgrades.MaxArmorUpgrade (SimpleMemberAccessExpression)
%4 = cmpi "slt", %1, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :55 :16)
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :55 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SubtractMoney
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :57 :34) // Not a variable of known type: upgradeCosts
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :57 :47) // Not a variable of known type: playerEntityStatsUpgrades
%7 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :57 :47) // playerEntityStatsUpgrades.CurrentArmorUpgrade (SimpleMemberAccessExpression)
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :57 :34) // upgradeCosts[playerEntityStatsUpgrades.CurrentArmorUpgrade] (ElementAccessExpression)
%9 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :57 :20) // SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentArmorUpgrade]) (InvocationExpression)
cond_br %9, ^3, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :57 :20)

^3: // SimpleBlock
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :59 :20) // Not a variable of known type: playerEntityStatsUpgrades
%11 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :59 :20) // playerEntityStatsUpgrades.CurrentArmorUpgrade (SimpleMemberAccessExpression)
%12 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :59 :69)
%13 = addi %11, %12 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :59 :20)
// No identifier name for binary assignment expression
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :60 :20) // Not a variable of known type: playerEntity
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :60 :20) // playerEntity.EntityMaxArmor (SimpleMemberAccessExpression)
%16 = constant 25 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :60 :51)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :60 :20) // Binary expression on unsupported types playerEntity.EntityMaxArmor += 25
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InvokeStatsUpgrade
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :61 :20) // InvokeStatsUpgrade() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerStatsManager.UpgradeDamage$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :66 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :68 :16) // Not a variable of known type: playerEntityStatsUpgrades
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :68 :16) // playerEntityStatsUpgrades.CurrentDamageUpgrade (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :68 :65) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :68 :65) // playerEntityStatsUpgrades.MaxDamageUpgrade (SimpleMemberAccessExpression)
%4 = cmpi "slt", %1, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :68 :16)
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :68 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SubtractMoney
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :70 :34) // Not a variable of known type: upgradeCosts
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :70 :47) // Not a variable of known type: playerEntityStatsUpgrades
%7 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :70 :47) // playerEntityStatsUpgrades.CurrentDamageUpgrade (SimpleMemberAccessExpression)
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :70 :34) // upgradeCosts[playerEntityStatsUpgrades.CurrentDamageUpgrade] (ElementAccessExpression)
%9 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :70 :20) // SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentDamageUpgrade]) (InvocationExpression)
cond_br %9, ^3, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :70 :20)

^3: // SimpleBlock
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :72 :20) // Not a variable of known type: playerEntityStatsUpgrades
%11 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :72 :20) // playerEntityStatsUpgrades.CurrentDamageUpgrade (SimpleMemberAccessExpression)
%12 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :72 :70)
%13 = addi %11, %12 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :72 :20)
// No identifier name for binary assignment expression
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :73 :20) // Not a variable of known type: playerEntity
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :73 :20) // playerEntity.MaxDamageModifier (SimpleMemberAccessExpression)
%16 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :73 :54) // 0.2f (NumericLiteralExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :73 :20) // Binary expression on unsupported types playerEntity.MaxDamageModifier += 0.2f
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InvokeStatsUpgrade
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :74 :20) // InvokeStatsUpgrade() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerStatsManager.UpgradeMovementSpeed$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :79 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :81 :16) // Not a variable of known type: playerEntityStatsUpgrades
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :81 :16) // playerEntityStatsUpgrades.CurrentMovementUpgrade (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :81 :67) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :81 :67) // playerEntityStatsUpgrades.MaxMovementUpgrade (SimpleMemberAccessExpression)
%4 = cmpi "slt", %1, %3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :81 :16)
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :81 :16)

^1: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SubtractMoney
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :83 :34) // Not a variable of known type: upgradeCosts
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :83 :47) // Not a variable of known type: playerEntityStatsUpgrades
%7 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :83 :47) // playerEntityStatsUpgrades.CurrentMovementUpgrade (SimpleMemberAccessExpression)
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :83 :34) // upgradeCosts[playerEntityStatsUpgrades.CurrentMovementUpgrade] (ElementAccessExpression)
%9 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :83 :20) // SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentMovementUpgrade]) (InvocationExpression)
cond_br %9, ^3, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :83 :20)

^3: // SimpleBlock
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :85 :20) // Not a variable of known type: playerEntityStatsUpgrades
%11 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :85 :20) // playerEntityStatsUpgrades.CurrentMovementUpgrade (SimpleMemberAccessExpression)
%12 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :85 :72)
%13 = addi %11, %12 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :85 :20)
// No identifier name for binary assignment expression
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :86 :20) // Not a variable of known type: playerEntity
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :86 :20) // playerEntity.EntityMaxMovementSpeed (SimpleMemberAccessExpression)
%16 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :86 :59) // 0.5f (NumericLiteralExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :86 :20) // Binary expression on unsupported types playerEntity.EntityMaxMovementSpeed += 0.5f
// No identifier name for binary assignment expression
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InvokeStatsUpgrade
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :87 :20) // InvokeStatsUpgrade() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.PlayerStatsManager.InvokeStatsUpgrade$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :92 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :94 :16) // Not a variable of known type: OnStatsUpgrade
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :94 :34) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :94 :16) // comparison of unknown type: OnStatsUpgrade != null
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :94 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :96 :16) // Not a variable of known type: OnStatsUpgrade
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\PlayerStatsManager.cs" :96 :16) // OnStatsUpgrade.Invoke() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
