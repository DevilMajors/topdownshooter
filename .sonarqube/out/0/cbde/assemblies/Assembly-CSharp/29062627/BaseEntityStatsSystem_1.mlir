func @_Major.EntitySystem.BaseEntityStatsSystem.DamageEntity$Major.EntitySystem.BaseEntity.float$(none, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :10 :8) {
^entry (%__entity : none, %__dmgAmount : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :10 :41)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :10 :41)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :10 :61)
cbde.store %__dmgAmount, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :10 :61)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :12 :16) // Not a variable of known type: _entity
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :12 :27) // null (NullLiteralExpression)
%4 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :12 :16) // comparison of unknown type: _entity != null
cond_br %4, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :12 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DamageEntityWithArmor
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :14 :38) // Not a variable of known type: _entity
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :14 :47) // Not a variable of known type: _dmgAmount
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :14 :16) // DamageEntityWithArmor(_entity, _dmgAmount) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: PassDataToHealthBar
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :15 :36) // Not a variable of known type: _entity
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :15 :16) // PassDataToHealthBar(_entity) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.BaseEntityStatsSystem.GiveHealthEntity$Major.EntitySystem.BaseEntity.int$(none, i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :19 :8) {
^entry (%__entity : none, %__healAmount : i32):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :19 :45)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :19 :45)
%1 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :19 :65)
cbde.store %__healAmount, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :19 :65)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :16) // Not a variable of known type: _entity
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :16) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%4 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :46)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :16) // Binary expression on unsupported types _entity.EntityCurrentHealth + _healAmount
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :60) // Not a variable of known type: _entity
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :60) // _entity.EntityMaxHealth (SimpleMemberAccessExpression)
%8 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :16) // comparison of unknown type: _entity.EntityCurrentHealth + _healAmount > _entity.EntityMaxHealth
cond_br %8, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :21 :16)

^1: // SimpleBlock
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :23 :16) // Not a variable of known type: _entity
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :23 :16) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :23 :46) // Not a variable of known type: _entity
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :23 :46) // _entity.EntityMaxHealth (SimpleMemberAccessExpression)
br ^3

^2: // SimpleBlock
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :27 :16) // Not a variable of known type: _entity
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :27 :16) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%15 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :27 :47)
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :27 :16) // Binary expression on unsupported types _entity.EntityCurrentHealth += _healAmount
// No identifier name for binary assignment expression
br ^3

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: PassDataToHealthBar
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :29 :32) // Not a variable of known type: _entity
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :29 :12) // PassDataToHealthBar(_entity) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Major.EntitySystem.BaseEntityStatsSystem.GiveArmorEntity$Major.EntitySystem.BaseEntity.int$(none, i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :32 :8) {
^entry (%__entity : none, %__armorAmount : i32):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :32 :44)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :32 :44)
%1 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :32 :64)
cbde.store %__armorAmount, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :32 :64)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :16) // Not a variable of known type: _entity
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :16) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%4 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :45)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :16) // Binary expression on unsupported types _entity.EntityCurrentArmor + _armorAmount
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :60) // Not a variable of known type: _entity
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :60) // _entity.EntityMaxArmor (SimpleMemberAccessExpression)
%8 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :16) // comparison of unknown type: _entity.EntityCurrentArmor + _armorAmount > _entity.EntityMaxArmor
cond_br %8, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :34 :16)

^1: // SimpleBlock
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :36 :16) // Not a variable of known type: _entity
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :36 :16) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :36 :45) // Not a variable of known type: _entity
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :36 :45) // _entity.EntityMaxArmor (SimpleMemberAccessExpression)
br ^3

^2: // SimpleBlock
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :40 :16) // Not a variable of known type: _entity
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :40 :16) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%15 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :40 :46)
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :40 :16) // Binary expression on unsupported types _entity.EntityCurrentArmor += _armorAmount
// No identifier name for binary assignment expression
br ^3

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: PassDataToHealthBar
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :43 :32) // Not a variable of known type: _entity
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :43 :12) // PassDataToHealthBar(_entity) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Major.EntitySystem.BaseEntityStatsSystem.Die$Major.EntitySystem.BaseEntity$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :46 :8) {
^entry (%__entity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :46 :32)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :46 :32)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :48 :12) // Not a variable of known type: _entity
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :48 :12) // _entity.IsDead (SimpleMemberAccessExpression)
%3 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :48 :29) // true
// Entity from another assembly: Destroy
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :49 :20) // Identifier from another assembly: gameObject
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :49 :12) // Destroy(gameObject) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.BaseEntityStatsSystem.DamageEntityWithArmor$Major.EntitySystem.BaseEntity.float$(none, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :52 :8) {
^entry (%__entity : none, %__dmgAmount : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :52 :43)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :52 :43)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :52 :63)
cbde.store %__dmgAmount, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :52 :63)
br ^0

^0: // BinaryBranchBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :54 :31) // Not a variable of known type: _dmgAmount
%3 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :54 :44) // 1.5f (NumericLiteralExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :54 :31) // Binary expression on unsupported types _dmgAmount / 1.5f
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :56 :16) // Not a variable of known type: _entity
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :56 :16) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%8 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :56 :45)
%9 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :56 :16) // comparison of unknown type: _entity.EntityCurrentArmor > 0
cond_br %9, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :56 :16)

^1: // BinaryBranchBlock
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :58 :34) // Not a variable of known type: _entity
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :58 :34) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :58 :63) // Not a variable of known type: dmgToArmor
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :58 :34) // Binary expression on unsupported types _entity.EntityCurrentArmor - dmgToArmor
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :59 :16) // Not a variable of known type: _entity
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :59 :16) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :59 :46) // Not a variable of known type: dmgToArmor
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :59 :16) // Binary expression on unsupported types _entity.EntityCurrentArmor -= dmgToArmor
// No identifier name for binary assignment expression
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :16) // Not a variable of known type: _entity
%20 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :16) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
// Entity from another assembly: Math
%21 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :63) // Not a variable of known type: _entity
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :63) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%23 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :91)
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :52) // Math.Round(_entity.EntityCurrentArmor, 1) (InvocationExpression)
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :60 :45) // (float)Math.Round(_entity.EntityCurrentArmor, 1) (CastExpression)
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :61 :20) // Not a variable of known type: _entity
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :61 :20) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%28 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :61 :49)
%29 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :61 :20) // comparison of unknown type: _entity.EntityCurrentArmor < 0
cond_br %29, ^3, ^4 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :61 :20)

^3: // SimpleBlock
%30 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :63 :20) // Not a variable of known type: _entity
%31 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :63 :20) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%32 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :63 :49)
%33 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :20) // Not a variable of known type: _entity
%34 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :20) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%35 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :51) // Not a variable of known type: tmpDamage
%36 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :64) // 1.5f (NumericLiteralExpression)
%37 = cbde.neg %36 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :63)
%38 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :51) // Binary expression on unsupported types tmpDamage * -1.5f
%39 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :64 :20) // Binary expression on unsupported types _entity.EntityCurrentHealth -= tmpDamage * -1.5f
// No identifier name for binary assignment expression
br ^4

^2: // BinaryBranchBlock
%40 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :20) // Not a variable of known type: _entity
%41 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :20) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%42 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :50) // Not a variable of known type: _dmgAmount
%43 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :20) // Binary expression on unsupported types _entity.EntityCurrentHealth - _dmgAmount
%44 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :63)
%45 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :20) // comparison of unknown type: _entity.EntityCurrentHealth - _dmgAmount > 0
cond_br %45, ^5, ^6 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :69 :20)

^5: // SimpleBlock
%46 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :71 :20) // Not a variable of known type: _entity
%47 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :71 :20) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%48 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :71 :51) // Not a variable of known type: _dmgAmount
%49 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :71 :20) // Binary expression on unsupported types _entity.EntityCurrentHealth -= _dmgAmount
// No identifier name for binary assignment expression
%50 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :20) // Not a variable of known type: _entity
%51 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :20) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
// Entity from another assembly: Math
%52 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :68) // Not a variable of known type: _entity
%53 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :68) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%54 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :97)
%55 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :57) // Math.Round(_entity.EntityCurrentHealth, 1) (InvocationExpression)
%56 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :72 :50) // (float)Math.Round(_entity.EntityCurrentHealth, 1) (CastExpression)
br ^4

^6: // SimpleBlock
%57 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :76 :20) // Not a variable of known type: _entity
%58 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :76 :20) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%59 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :76 :50)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: Die
%60 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :77 :24) // Not a variable of known type: _entity
%61 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :77 :20) // Die(_entity) (InvocationExpression)
br ^4

^4: // ExitBlock
return

}
func @_Major.EntitySystem.BaseEntityStatsSystem.PassDataToHealthBar$Major.EntitySystem.BaseEntity$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :82 :8) {
^entry (%__entity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :82 :41)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :82 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :84 :16) // Not a variable of known type: OnHealthArmorEntityChanged
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :84 :46) // null (NullLiteralExpression)
%3 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :84 :16) // comparison of unknown type: OnHealthArmorEntityChanged != null
cond_br %3, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :84 :16)

^1: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :86 :38) // Not a variable of known type: _entity
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :86 :38) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :86 :68) // Not a variable of known type: _entity
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :86 :68) // _entity.EntityMaxHealth (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :86 :38) // Binary expression on unsupported types _entity.EntityCurrentHealth / _entity.EntityMaxHealth
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :87 :37) // Not a variable of known type: _entity
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :87 :37) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :87 :66) // Not a variable of known type: _entity
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :87 :66) // _entity.EntityMaxArmor (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :87 :37) // Binary expression on unsupported types _entity.EntityCurrentArmor / _entity.EntityMaxArmor
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :88 :16) // Not a variable of known type: OnHealthArmorEntityChanged
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :88 :43) // Not a variable of known type: healthPercent
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :88 :58) // Not a variable of known type: armorPercent
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :88 :16) // OnHealthArmorEntityChanged(healthPercent, armorPercent) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.EntitySystem.BaseEntityStatsSystem.GetEnemyHealth$Major.EntitySystem.BaseEntity$(none) -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :92 :8) {
^entry (%__entity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :92 :36)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :92 :36)
br ^0

^0: // JumpBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :94 :19) // Not a variable of known type: _entity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :94 :19) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
return %2 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\BaseEntityStatsSystem.cs" :94 :12)

^1: // ExitBlock
cbde.unreachable

}
