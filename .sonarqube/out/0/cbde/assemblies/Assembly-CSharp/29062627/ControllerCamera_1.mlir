func @_Major.GameControllers.ControllerCamera.Start$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :14 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :16 :16) // Not a variable of known type: cameraTarget
%1 = cbde.unknown : i1 // Creating necessary bool for conversion
cond_br %1, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :16 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CameraAction
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :18 :16) // CameraAction() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.GameControllers.ControllerCamera.FixedUpdate$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :21 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :23 :16) // Not a variable of known type: cameraTarget
%1 = cbde.unknown : i1 // Creating necessary bool for conversion
cond_br %1, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :23 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: CameraAction
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :25 :16) // CameraAction() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.GameControllers.ControllerCamera.CameraAction$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :29 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Vector3
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :36) // Vector3.forward (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :55) // Not a variable of known type: cameraDistance
%2 = cbde.neg %1 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :54)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :36) // Binary expression on unsupported types Vector3.forward * -cameraDistance
// Entity from another assembly: Vector3
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :72) // Vector3.up (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :85) // Not a variable of known type: cameraHeight
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :72) // Binary expression on unsupported types Vector3.up * cameraHeight
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :31 :36) // Binary expression on unsupported types Vector3.forward * -cameraDistance + Vector3.up * cameraHeight
// Entity from another assembly: Quaternion
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :32 :57) // Not a variable of known type: cameraAngle
// Entity from another assembly: Vector3
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :32 :70) // Vector3.up (SimpleMemberAccessExpression)
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :32 :36) // Quaternion.AngleAxis(cameraAngle, Vector3.up) (InvocationExpression)
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :32 :84) // Not a variable of known type: worldPosition
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :32 :36) // Binary expression on unsupported types Quaternion.AngleAxis(cameraAngle, Vector3.up) * worldPosition
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :33 :41) // Not a variable of known type: cameraTarget
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :33 :41) // cameraTarget.position (SimpleMemberAccessExpression)
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :34 :12) // Not a variable of known type: flatTargetPosition
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :34 :12) // flatTargetPosition.y (SimpleMemberAccessExpression)
%20 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :34 :35) // 0f (NumericLiteralExpression)
%21 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :36 :36) // Not a variable of known type: flatTargetPosition
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :36 :57) // Not a variable of known type: rotatedVector
%23 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :36 :36) // Binary expression on unsupported types flatTargetPosition + rotatedVector
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :12) // Identifier from another assembly: transform
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :12) // transform.position (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%27 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :52) // Identifier from another assembly: transform
%28 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :52) // transform.position (SimpleMemberAccessExpression)
%29 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :72) // Not a variable of known type: finalPosition
%30 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :102) // Not a variable of known type: cameraSmoothTime
%31 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :91) // Not a variable of known type: refVector
%32 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :37 :33) // Vector3.SmoothDamp(transform.position, finalPosition, ref refVector, cameraSmoothTime) (InvocationExpression)
%33 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :38 :12) // Identifier from another assembly: transform
%34 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :38 :29) // Not a variable of known type: flatTargetPosition
%35 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\ControllerCamera.cs" :38 :12) // transform.LookAt(flatTargetPosition) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
