func @_Major.EntitySystem.AttackMeleState.Tick$$() -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :17 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: FacePlayer
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :19 :12) // FacePlayer() (InvocationExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :20 :12) // Not a variable of known type: enemy
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :20 :12) // enemy.EnemyRigidbody (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :20 :12) // enemy.EnemyRigidbody.velocity (SimpleMemberAccessExpression)
// Entity from another assembly: Vector3
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :20 :44) // Vector3.zero (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :21 :16) // Not a variable of known type: attackDelay
%6 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :21 :16) // attackDelay.TimerCycle() (InvocationExpression)
cond_br %6, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :21 :16)

^1: // SimpleBlock
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :23 :16) // Not a variable of known type: enemy
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :23 :16) // enemy.animator (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :23 :39) // "isAttacking" (StringLiteralExpression)
%10 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :23 :54) // true
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :23 :16) // enemy.animator.SetBool("isAttacking", true) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: MeleAttackPlayer
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :24 :16) // MeleAttackPlayer() (InvocationExpression)
br ^2

^2: // JumpBlock
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :26 :19) // typeof(AttackControllState) (TypeOfExpression)
return %13 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :26 :12)

^3: // ExitBlock
cbde.unreachable

}
func @_Major.EntitySystem.AttackMeleState.MeleAttackPlayer$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :29 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetAndDamagePlayer
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :31 :31) // Not a variable of known type: enemy
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :31 :31) // enemy.attackPoint (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :31 :12) // GetAndDamagePlayer(enemy.attackPoint) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.EntitySystem.AttackMeleState.FacePlayer$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :34 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :36 :27) // Not a variable of known type: enemy
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :36 :27) // enemy.Target (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :36 :27) // enemy.Target.position (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :36 :51) // Not a variable of known type: transform
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :36 :51) // transform.position (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :36 :27) // Binary expression on unsupported types enemy.Target.position - transform.position
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :37 :12) // Not a variable of known type: rotation
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :37 :12) // rotation.y (SimpleMemberAccessExpression)
%9 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :37 :25)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :38 :12) // Not a variable of known type: transform
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :38 :12) // transform.rotation (SimpleMemberAccessExpression)
// Entity from another assembly: Quaternion
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :38 :57) // Not a variable of known type: rotation
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\AttackMeleState.cs" :38 :33) // Quaternion.LookRotation(rotation) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function GetAndDamagePlayer(none), it contains poisonous unsupported syntaxes

