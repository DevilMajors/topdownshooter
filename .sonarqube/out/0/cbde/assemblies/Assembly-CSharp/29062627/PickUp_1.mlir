// Skipping function OnTriggerEnter(none), it contains poisonous unsupported syntaxes

func @_Major.Level.Triggers.PickUp.RandomizePickUpAmount$int.int$(i32, i32) -> i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :32 :8) {
^entry (%_min : i32, %_max : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :32 :42)
cbde.store %_min, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :32 :42)
%1 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :32 :51)
cbde.store %_max, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :32 :51)
br ^0

^0: // JumpBlock
// Entity from another assembly: Random
%2 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :34 :32)
%3 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :34 :37)
%4 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :34 :19) // Random.Range(min, max) (InvocationExpression)
return %4 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\StageScripts\\PickUp.cs" :34 :12)

^1: // ExitBlock
cbde.unreachable

}
