func @_Major.WeaponSystem.Bullet.OnEnable$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :18 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :20 :36) // 0f (NumericLiteralExpression)
%1 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :21 :42)
%2 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :21 :45)
%3 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :21 :48)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :21 :30) // new Vector3(0, 0, 0) (ObjectCreationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function OnTriggerEnter(none), it contains poisonous unsupported syntaxes

func @_Major.WeaponSystem.Bullet.BulletTimeExpire$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :53 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :55 :16) // Not a variable of known type: bulletCurrentLifeTime
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :55 :41) // Not a variable of known type: bulletMaxLifeTime
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :55 :16) // comparison of unknown type: bulletCurrentLifeTime >= bulletMaxLifeTime
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :55 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameObjectPool
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :57 :16) // GameObjectPool.Instance (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :57 :53) // Identifier from another assembly: gameObject
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :57 :16) // GameObjectPool.Instance.ReturnToPool(gameObject) (InvocationExpression)
br ^3

^2: // BinaryBranchBlock
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :16) // Not a variable of known type: distanceCovered
// Entity from another assembly: Vector3
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :35) // Vector3.forward (SimpleMemberAccessExpression)
// Entity from another assembly: Time
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :53) // Time.deltaTime (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :35) // Binary expression on unsupported types Vector3.forward * Time.deltaTime
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :70) // Not a variable of known type: bulletSpeed
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :35) // Binary expression on unsupported types Vector3.forward * Time.deltaTime * bulletSpeed
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :61 :16) // Binary expression on unsupported types distanceCovered += Vector3.forward * Time.deltaTime * bulletSpeed
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :16) // Identifier from another assembly: transform
// Entity from another assembly: Vector3
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :36) // Vector3.forward (SimpleMemberAccessExpression)
// Entity from another assembly: Time
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :54) // Time.deltaTime (SimpleMemberAccessExpression)
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :36) // Binary expression on unsupported types Vector3.forward * Time.deltaTime
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :71) // Not a variable of known type: bulletSpeed
%18 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :36) // Binary expression on unsupported types Vector3.forward * Time.deltaTime * bulletSpeed
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :62 :16) // transform.Translate(Vector3.forward * Time.deltaTime * bulletSpeed) (InvocationExpression)
%20 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :64 :20) // Not a variable of known type: distanceCovered
%21 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :64 :20) // distanceCovered.magnitude (SimpleMemberAccessExpression)
%22 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :64 :49) // Not a variable of known type: rangeOfLife
%23 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :64 :20) // comparison of unknown type: distanceCovered.magnitude >= rangeOfLife
cond_br %23, ^4, ^3 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :64 :20)

^4: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GameObjectPool
%24 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :66 :20) // GameObjectPool.Instance (SimpleMemberAccessExpression)
%25 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :66 :57) // Identifier from another assembly: gameObject
%26 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :66 :20) // GameObjectPool.Instance.ReturnToPool(gameObject) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Major.WeaponSystem.Bullet.Update$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :71 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ManagerGame
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :73 :17) // ManagerGame.Instance (SimpleMemberAccessExpression)
%1 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :73 :17) // ManagerGame.Instance.gameIsPaused (SimpleMemberAccessExpression)
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :73 :16) // !ManagerGame.Instance.gameIsPaused (LogicalNotExpression)
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :73 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :75 :16) // Not a variable of known type: bulletCurrentLifeTime
// Entity from another assembly: Time
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :75 :41) // Time.deltaTime (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :75 :16) // Binary expression on unsupported types bulletCurrentLifeTime += Time.deltaTime
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: BulletTimeExpire
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\Bullet.cs" :76 :16) // BulletTimeExpire() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
