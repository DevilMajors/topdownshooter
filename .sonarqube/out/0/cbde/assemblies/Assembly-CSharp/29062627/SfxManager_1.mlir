func @_Major.GameManagers.SFXManager.SfxManager.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :12 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :14 :16) // Not a variable of known type: Instance
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :14 :28) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :14 :16) // comparison of unknown type: Instance == null
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :14 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :16 :27) // this (ThisExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.GameManagers.SFXManager.SfxManager.PlaySFX$string.float.UnityEngine.Transform$(none, none, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :8) {
^entry (%__sfxName : none, %__playTime : none, %__sfxPlace : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :28)
cbde.store %__sfxName, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :28)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :45)
cbde.store %__playTime, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :45)
%2 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :62)
cbde.store %__sfxPlace, %2 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :20 :62)
br ^0

^0: // BinaryBranchBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GetSFXNumber
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :22 :41) // Not a variable of known type: _sfxName
%4 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :22 :28) // GetSFXNumber(_sfxName) (InvocationExpression)
%5 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :22 :16) // sfxNumber
cbde.store %4, %5 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :22 :16)
%6 = cbde.load %5 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :23 :16)
%7 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :23 :30)
%8 = cbde.neg %7 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :23 :29)
%9 = cmpi "ne", %6, %8 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :23 :16)
cond_br %9, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :23 :16)

^1: // SimpleBlock
// Entity from another assembly: Instantiate
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :52) // Not a variable of known type: effectsList
%11 = cbde.load %5 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :64)
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :52) // effectsList[sfxNumber] (ElementAccessExpression)
%13 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :52) // effectsList[sfxNumber].SfxGameObject (SimpleMemberAccessExpression)
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :90) // Not a variable of known type: _sfxPlace
%15 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :90) // _sfxPlace.position (SimpleMemberAccessExpression)
// Entity from another assembly: Quaternion
%16 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :110) // Quaternion.identity (SimpleMemberAccessExpression)
%17 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :25 :40) // Instantiate(effectsList[sfxNumber].SfxGameObject, _sfxPlace.position, Quaternion.identity) (InvocationExpression)
// Entity from another assembly: Destroy
%19 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :26 :24) // Not a variable of known type: tempEffect
%20 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :26 :36) // Not a variable of known type: _playTime
%21 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\SfxManager.cs" :26 :16) // Destroy(tempEffect, _playTime) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
// Skipping function GetSFXNumber(none), it contains poisonous unsupported syntaxes

