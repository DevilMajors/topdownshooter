// Skipping function Tick(), it contains poisonous unsupported syntaxes

// Skipping function ExplodeOnDeath(none), it contains poisonous unsupported syntaxes

func @_Major.EntitySystem.DieState.GetEnemiesInExplosionRange$float$(none) -> none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :64 :8) {
^entry (%__damageRange : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :64 :54)
cbde.store %__damageRange, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :64 :54)
br ^0

^0: // JumpBlock
// Entity from another assembly: Physics
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :66 :41) // Not a variable of known type: transform
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :66 :41) // transform.position (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :66 :61) // Not a variable of known type: _damageRange
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :66 :19) // Physics.OverlapSphere(transform.position, _damageRange) (InvocationExpression)
return %4 : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\EntitiesSystem\\Enemies\\FSM States\\DieState.cs" :66 :12)

^1: // ExitBlock
cbde.unreachable

}
