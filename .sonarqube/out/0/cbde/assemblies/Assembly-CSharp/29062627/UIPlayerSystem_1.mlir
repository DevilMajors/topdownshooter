func @_Major.UI.UIPlayerSystem.Start$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :23 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :25 :12) // Not a variable of known type: reloadBar
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :25 :12) // reloadBar.gameObject (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :25 :43) // false
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :25 :12) // reloadBar.gameObject.SetActive(false) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :26 :27) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :26 :27) // GlobalDataManager.Instance.playerEntity (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :27 :40) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :27 :40) // GlobalDataManager.Instance.playerEntityStatsUpgrades (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerStatsUpgrade
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :28 :12) // OnPlayerStatsUpgrade() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnPlayerHealthModification$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :31 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :12) // Not a variable of known type: playerHealthText
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :12) // playerHealthText.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :36) // "Health: " (StringLiteralExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :49) // Not a variable of known type: playerEntity
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :49) // playerEntity.EntityCurrentHealth (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :36) // Binary expression on unsupported types "Health: " + playerEntity.EntityCurrentHealth
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :84) // "/" (StringLiteralExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :36) // Binary expression on unsupported types "Health: " + playerEntity.EntityCurrentHealth + "/"
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :90) // Not a variable of known type: playerEntity
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :90) // playerEntity.EntityMaxHealth (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :33 :36) // Binary expression on unsupported types "Health: " + playerEntity.EntityCurrentHealth + "/" + playerEntity.EntityMaxHealth
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnPlayerArmorModification$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :36 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :12) // Not a variable of known type: playerArmorText
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :12) // playerArmorText.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :35) // "Armor: " (StringLiteralExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :47) // Not a variable of known type: playerEntity
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :47) // playerEntity.EntityCurrentArmor (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :35) // Binary expression on unsupported types "Armor: " + playerEntity.EntityCurrentArmor
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :81) // "/" (StringLiteralExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :35) // Binary expression on unsupported types "Armor: " + playerEntity.EntityCurrentArmor + "/"
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :87) // Not a variable of known type: playerEntity
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :87) // playerEntity.EntityMaxArmor (SimpleMemberAccessExpression)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :38 :35) // Binary expression on unsupported types "Armor: " + playerEntity.EntityCurrentArmor + "/" + playerEntity.EntityMaxArmor
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnPlayerMoneyModification$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :41 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :43 :12) // Not a variable of known type: playerMoneyText
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :43 :12) // playerMoneyText.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :43 :35) // "Money: " (StringLiteralExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :43 :47) // Not a variable of known type: playerEntity
%4 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :43 :47) // playerEntity.MoneyAmount (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :43 :35) // Binary expression on unsupported types "Money: " + playerEntity.MoneyAmount
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnUpgradeHealth$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :46 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :48 :12) // Not a variable of known type: upgradeHealth
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :48 :12) // upgradeHealth.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :48 :33) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :48 :33) // playerEntityStatsUpgrades.CurrentHealthUpgrade (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :48 :82) // "/5" (StringLiteralExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :48 :33) // Binary expression on unsupported types playerEntityStatsUpgrades.CurrentHealthUpgrade + "/5"
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnUpgradeArmor$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :51 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :53 :12) // Not a variable of known type: upgradeArmor
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :53 :12) // upgradeArmor.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :53 :32) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :53 :32) // playerEntityStatsUpgrades.CurrentArmorUpgrade (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :53 :80) // "/5" (StringLiteralExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :53 :32) // Binary expression on unsupported types playerEntityStatsUpgrades.CurrentArmorUpgrade + "/5"
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnUpgradeDamage$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :56 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :58 :12) // Not a variable of known type: upgradeDamage
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :58 :12) // upgradeDamage.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :58 :33) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :58 :33) // playerEntityStatsUpgrades.CurrentDamageUpgrade (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :58 :82) // "/5" (StringLiteralExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :58 :33) // Binary expression on unsupported types playerEntityStatsUpgrades.CurrentDamageUpgrade + "/5"
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnUpgradeMs$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :61 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :63 :12) // Not a variable of known type: upgradeMs
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :63 :12) // upgradeMs.text (SimpleMemberAccessExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :63 :29) // Not a variable of known type: playerEntityStatsUpgrades
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :63 :29) // playerEntityStatsUpgrades.CurrentMovementUpgrade (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :63 :80) // "/5" (StringLiteralExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :63 :29) // Binary expression on unsupported types playerEntityStatsUpgrades.CurrentMovementUpgrade + "/5"
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.DisplayReloadBar$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :66 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :68 :12) // Not a variable of known type: reloadBar
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :68 :12) // reloadBar.gameObject (SimpleMemberAccessExpression)
%2 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :68 :43) // true
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :68 :12) // reloadBar.gameObject.SetActive(true) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.HideReloadBar$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :71 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :73 :12) // Not a variable of known type: reloadBar
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :73 :12) // reloadBar.gameObject (SimpleMemberAccessExpression)
%2 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :73 :43) // false
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :73 :12) // reloadBar.gameObject.SetActive(false) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function ReloadBarTime(none), it contains poisonous unsupported syntaxes

func @_Major.UI.UIPlayerSystem.OnPlayerAmmoModification$int.int$(i32, i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :89 :8) {
^entry (%__bulletsLeft : i32, %__magazineSize : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :89 :45)
cbde.store %__bulletsLeft, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :89 :45)
%1 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :89 :63)
cbde.store %__magazineSize, %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :89 :63)
br ^0

^0: // SimpleBlock
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :12) // Not a variable of known type: playerAmmoText
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :12) // playerAmmoText.text (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :34) // "Ammo: " (StringLiteralExpression)
%5 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :45)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :34) // Binary expression on unsupported types "Ammo: " + _bulletsLeft
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :60) // "/" (StringLiteralExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :34) // Binary expression on unsupported types "Ammo: " + _bulletsLeft + "/"
%9 = cbde.load %1 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :66)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :91 :34) // Binary expression on unsupported types "Ammo: " + _bulletsLeft + "/" + _magazineSize
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.HandleReload$float$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :94 :8) {
^entry (%__time : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :94 :33)
cbde.store %__time, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :94 :33)
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: DisplayReloadBar
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :96 :12) // DisplayReloadBar() (InvocationExpression)
// Entity from another assembly: StartCoroutine
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ReloadBarTime
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :97 :41) // Not a variable of known type: _time
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :97 :27) // ReloadBarTime(_time) (InvocationExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :97 :12) // StartCoroutine(ReloadBarTime(_time)) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnPlayerStatsModification$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :100 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerHealthModification
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :102 :12) // OnPlayerHealthModification() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerArmorModification
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :103 :12) // OnPlayerArmorModification() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnPlayerMoneyModification
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :104 :12) // OnPlayerMoneyModification() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIPlayerSystem.OnPlayerStatsUpgrade$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :107 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnUpgradeArmor
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :109 :12) // OnUpgradeArmor() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnUpgradeDamage
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :110 :12) // OnUpgradeDamage() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnUpgradeHealth
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :111 :12) // OnUpgradeHealth() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: OnUpgradeMs
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIPlayerSystem.cs" :112 :12) // OnUpgradeMs() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
