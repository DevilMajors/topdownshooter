// Skipping function Awake(), it contains poisonous unsupported syntaxes

func @_Major.GameManagers.ManagerGame.InitializeScene$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :37 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :39 :16) // Not a variable of known type: uiStageInfo
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :39 :31) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :39 :16) // comparison of unknown type: uiStageInfo == null
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :39 :16)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :41 :30) // GetComponent<UIStageInfo>() (InvocationExpression)
br ^2

^2: // SimpleBlock
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :43 :12) // Not a variable of known type: loadingScreen
%5 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :43 :36) // false
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :43 :12) // loadingScreen.SetActive(false) (InvocationExpression)
// Entity from another assembly: SceneManager
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :44 :27) // SceneManager.GetActiveScene() (InvocationExpression)
%8 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :44 :27) // SceneManager.GetActiveScene().buildIndex (SimpleMemberAccessExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LoadSceneByNumber
%9 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :46 :30)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :46 :12) // LoadSceneByNumber(1) (InvocationExpression)
br ^3

^3: // ExitBlock
return

}
func @_Major.GameManagers.ManagerGame.SceneExists$int$(i32) -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :49 :8) {
^entry (%__sceneNumber : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :49 :33)
cbde.store %__sceneNumber, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :49 :33)
br ^0

^0: // JumpBlock
// Entity from another assembly: Application
%1 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :51 :56)
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :51 :19) // Application.CanStreamedLevelBeLoaded(_sceneNumber) (InvocationExpression)
return %2 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :51 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Major.GameManagers.ManagerGame.SceneIsLoaded$int$(i32) -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :54 :8) {
^entry (%__sceneNumber : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :54 :35)
cbde.store %__sceneNumber, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :54 :35)
br ^0

^0: // JumpBlock
// Entity from another assembly: SceneManager
%1 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :56 :53)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :56 :19) // SceneManager.GetSceneByBuildIndex(_sceneNumber) (InvocationExpression)
%3 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :56 :19) // SceneManager.GetSceneByBuildIndex(_sceneNumber).isLoaded (SimpleMemberAccessExpression)
return %3 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :56 :12)

^1: // ExitBlock
cbde.unreachable

}
// Skipping function LoadSceneByNumber(i32), it contains poisonous unsupported syntaxes

func @_Major.GameManagers.ManagerGame.StageCompleteTrigger$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :82 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :84 :12) // Not a variable of known type: uiStageInfo
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :84 :45) // Not a variable of known type: currentScene
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :84 :12) // uiStageInfo.MarkStageAsCompleted(currentScene) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: LoadSceneByNumber
%3 = constant 2 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :85 :30)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :85 :12) // LoadSceneByNumber(2) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameManagers.ManagerGame.PauseGame$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :88 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :90 :27) // true
// Entity from another assembly: Time
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :91 :12) // Time.timeScale (SimpleMemberAccessExpression)
%2 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :91 :29) // 0f (NumericLiteralExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameManagers.ManagerGame.UnpauseGame$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :94 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :96 :27) // false
// Entity from another assembly: Time
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :97 :12) // Time.timeScale (SimpleMemberAccessExpression)
%2 = constant unit loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :97 :29) // 1f (NumericLiteralExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameManagers.ManagerGame.QuitApplication$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :100 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Entity from another assembly: Application
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\ManagerGame.cs" :102 :12) // Application.Quit() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
// Skipping function GetLoadProgress(), it contains poisonous unsupported syntaxes

