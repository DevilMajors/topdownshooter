func @_Major.GameManagers.RespawnController.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :18 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :20 :15) // Not a variable of known type: Instance
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :20 :27) // null (NullLiteralExpression)
%2 = cbde.unknown : i1  loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :20 :15) // comparison of unknown type: Instance == null
cond_br %2, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :20 :15)

^1: // SimpleBlock
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :22 :27) // this (ThisExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.GameManagers.RespawnController.Update$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :26 :8) {
^entry :
br ^0

^0: // BinaryBranchBlock
%0 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :28 :16) // Not a variable of known type: isDeadTimerCounting
cond_br %0, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :28 :16)

^1: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :30 :20) // Not a variable of known type: respawnTimer
%2 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :30 :20) // respawnTimer.TimerExpired() (InvocationExpression)
cond_br %2, ^3, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :30 :20)

^3: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: ResetEntityStats
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :32 :37) // Not a variable of known type: playerEntity
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :32 :20) // ResetEntityStats(playerEntity) (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetPlayerActive
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :33 :36) // Not a variable of known type: playerGameObject
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :33 :20) // SetPlayerActive(playerGameObject) (InvocationExpression)
%7 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :34 :42) // false
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :35 :20) // Not a variable of known type: OnPlayerRespawn
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :35 :20) // OnPlayerRespawn.Invoke() (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
func @_Major.GameManagers.RespawnController.RespawnPlayer$Major.EntitySystem.BaseEntity.UnityEngine.GameObject$(none, none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :40 :8) {
^entry (%__entity : none, %__gameObject : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :40 :34)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :40 :34)
%1 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :40 :54)
cbde.store %__gameObject, %1 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :40 :54)
br ^0

^0: // SimpleBlock
%2 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :42 :34) // true
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :43 :27) // Not a variable of known type: _entity
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :44 :31) // Not a variable of known type: _gameObject
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :45 :12) // Not a variable of known type: respawnTimer
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :45 :12) // respawnTimer.RestartTimer() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.GameManagers.RespawnController.ResetEntityStats$Major.EntitySystem.BaseEntity$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :48 :8) {
^entry (%__entity : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :48 :38)
cbde.store %__entity, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :48 :38)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :50 :12) // Not a variable of known type: _entity
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :50 :12) // _entity.EntityCurrentHealth (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :50 :42) // Not a variable of known type: _entity
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :50 :42) // _entity.EntityMaxHealth (SimpleMemberAccessExpression)
%5 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :51 :12) // Not a variable of known type: _entity
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :51 :12) // _entity.EntityCurrentArmor (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :51 :41) // Not a variable of known type: _entity
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :51 :41) // _entity.EntityMaxArmor (SimpleMemberAccessExpression)
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :52 :12) // Not a variable of known type: _entity
%10 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :52 :12) // _entity.IsDead (SimpleMemberAccessExpression)
%11 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :52 :29) // false
br ^1

^1: // ExitBlock
return

}
func @_Major.GameManagers.RespawnController.SetPlayerActive$UnityEngine.GameObject$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :55 :8) {
^entry (%__playerGameObject : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :55 :37)
cbde.store %__playerGameObject, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :55 :37)
br ^0

^0: // SimpleBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :57 :12) // Not a variable of known type: _playerGameObject
%2 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :57 :40) // true
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\RespawnController.cs" :57 :12) // _playerGameObject.SetActive(true) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
