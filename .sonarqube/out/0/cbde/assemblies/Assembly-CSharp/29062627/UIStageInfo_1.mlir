func @_Major.UI.UIStageInfo.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :17 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitStagesText
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :19 :12) // InitStagesText() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitStageTable
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :20 :12) // InitStageTable() (InvocationExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: InitStagesButtons
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :21 :12) // InitStagesButtons() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.UI.UIStageInfo.InitStageTable$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :24 :8) {
^entry :
br ^0

^0: // ForInitializerBlock
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :26 :25) // new Hashtable() (ObjectCreationExpression)
// Entity from another assembly: SceneManager
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :27 :27) // SceneManager.sceneCountInBuildSettings (SimpleMemberAccessExpression)
%2 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :25)
%3 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :21) // i
cbde.store %2, %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :21)
br ^1

^1: // BinaryBranchBlock
%4 = cbde.load %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :28)
%5 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :32) // Not a variable of known type: stagesAmount
%6 = constant 3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :47)
%7 = subi %5, %6 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :32)
%8 = cmpi "slt", %4, %7 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :28)
cond_br %8, ^2, ^3 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :28)

^2: // SimpleBlock
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :30 :16) // Not a variable of known type: stageTable
%10 = cbde.load %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :30 :31)
%11 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :30 :34) // false
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :30 :16) // stageTable.Add(i, false) (InvocationExpression)
br ^4

^4: // SimpleBlock
%13 = cbde.load %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :50)
%14 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :50)
%15 = addi %13, %14 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :50)
cbde.store %15, %3 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :28 :50)
br ^1

^3: // ExitBlock
return

}
// Skipping function InitStagesText(), it contains poisonous unsupported syntaxes

// Skipping function InitStagesButtons(), it contains poisonous unsupported syntaxes

func @_Major.UI.UIStageInfo.UnlockNextStage$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :57 :8) {
^entry (%__stageNumber : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :57 :37)
cbde.store %__stageNumber, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :57 :37)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :59 :16)
%2 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :59 :31)
%3 = addi %1, %2 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :59 :16)
%4 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :59 :35) // Not a variable of known type: stagesAmount
%5 = cmpi "slt", %3, %4 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :59 :16)
cond_br %5, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :59 :16)

^1: // SimpleBlock
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :16) // Not a variable of known type: levelsButtons
%7 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :30)
%8 = constant 1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :45)
%9 = addi %7, %8 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :30)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :16) // levelsButtons[_stageNumber + 1] (ElementAccessExpression)
%11 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :16) // levelsButtons[_stageNumber + 1].interactable (SimpleMemberAccessExpression)
%12 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :60 :63) // true
br ^2

^2: // ExitBlock
return

}
func @_Major.UI.UIStageInfo.MarkStageAsCompleted$int$(i32) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :63 :8) {
^entry (%__stageNumber : i32):
%0 = cbde.alloca i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :63 :41)
cbde.store %__stageNumber, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :63 :41)
br ^0

^0: // SimpleBlock
%1 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :65 :12)
%2 = constant 3 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :65 :28)
%3 = subi %1, %2 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :65 :12)
cbde.store %3, %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :65 :12)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :66 :12) // Not a variable of known type: stageTable
%5 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :66 :23)
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :66 :12) // stageTable[_stageNumber] (ElementAccessExpression)
%7 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :66 :39) // true
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :67 :12) // Not a variable of known type: completedText
%9 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :67 :26)
%10 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :67 :12) // completedText[_stageNumber] (ElementAccessExpression)
%11 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :67 :12) // completedText[_stageNumber].text (SimpleMemberAccessExpression)
%12 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :67 :47) // "Completed" (StringLiteralExpression)
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: UnlockNextStage
%13 = cbde.load %0 : memref<i32> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :68 :28)
%14 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\UIScripts\\UIStageInfo.cs" :68 :12) // UnlockNextStage(_stageNumber) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
