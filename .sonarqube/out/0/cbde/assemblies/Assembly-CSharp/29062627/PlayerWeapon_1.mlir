func @_Major.WeaponSystem.PlayerWeapon.OnEnable$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :21 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetBulletsOnUI
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :23 :12) // SetBulletsOnUI() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.PlayerWeapon.Awake$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :26 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: WeaponInit
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :28 :12) // WeaponInit() (InvocationExpression)
%1 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :29 :26) // Not a variable of known type: magazineSize
%2 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :30 :29) // true
br ^1

^1: // ExitBlock
return

}
// Skipping function ShootWeapon(), it contains poisonous unsupported syntaxes

func @_Major.WeaponSystem.PlayerWeapon.ReloadWeapon$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :61 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :63 :26) // true
// Entity from another assembly: Invoke
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :64 :19) // nameof(ReloadFinish) (InvocationExpression)
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :64 :41) // Not a variable of known type: reloadTime
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :64 :12) // Invoke(nameof(ReloadFinish), reloadTime) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.PlayerWeapon.ReloadFinish$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :67 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :69 :26) // Not a variable of known type: magazineSize
%1 = constant 0 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :70 :26) // false
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: SetBulletsOnUI
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :71 :12) // SetBulletsOnUI() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.PlayerWeapon.ResetShootWeapon$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :74 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :76 :29) // true
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.PlayerWeapon.HaveBulletsInMagazine$$() -> i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :79 :8) {
^entry :
br ^0

^0: // JumpBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :81 :19) // Not a variable of known type: bulletsLeft
%1 = constant 0 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :81 :33)
%2 = cmpi "sgt", %0, %1 : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :81 :19)
return %2 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :81 :12)

^1: // ExitBlock
cbde.unreachable

}
func @_Major.WeaponSystem.PlayerWeapon.SetBulletsOnUI$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :84 :8) {
^entry :
br ^0

^0: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%0 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :86 :12) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :86 :12) // GlobalDataManager.Instance.uIPlayerSystem (SimpleMemberAccessExpression)
%2 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :86 :79) // Not a variable of known type: bulletsLeft
%3 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :86 :92) // Not a variable of known type: magazineSize
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :86 :12) // GlobalDataManager.Instance.uIPlayerSystem.OnPlayerAmmoModification(bulletsLeft, magazineSize) (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
func @_Major.WeaponSystem.PlayerWeapon.WeaponInit$$() -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :89 :8) {
^entry :
br ^0

^0: // SimpleBlock
%0 = cbde.unknown : i32 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :91 :26) // Not a variable of known type: magazineSize
%1 = constant 1 : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :92 :29) // true
br ^1

^1: // ExitBlock
return

}
// Skipping function PlayerInput(), it contains poisonous unsupported syntaxes

func @_Major.WeaponSystem.PlayerWeapon.PassDataToReloadBar$float$(none) -> () loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :119 :8) {
^entry (%__time : none):
%0 = cbde.alloca none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :119 :41)
cbde.store %__time, %0 : memref<none> loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :119 :41)
br ^0

^0: // BinaryBranchBlock
%1 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :121 :16) // Identifier from another assembly: transform
%2 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :121 :16) // transform.parent (SimpleMemberAccessExpression)
%3 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :121 :16) // transform.parent.gameObject (SimpleMemberAccessExpression)
%4 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :121 :55) // "Player" (StringLiteralExpression)
%5 = cbde.unknown : i1 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :121 :16) // transform.parent.gameObject.CompareTag("Player") (InvocationExpression)
cond_br %5, ^1, ^2 loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :121 :16)

^1: // SimpleBlock
// Skipped because MethodDeclarationSyntax or ClassDeclarationSyntax or NamespaceDeclarationSyntax: GlobalDataManager
%6 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :123 :16) // GlobalDataManager.Instance (SimpleMemberAccessExpression)
%7 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :123 :16) // GlobalDataManager.Instance.uIPlayerSystem (SimpleMemberAccessExpression)
%8 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :123 :71) // Not a variable of known type: _time
%9 = cbde.unknown : none loc("C:\\Users\\matma\\Desktop\\IT Degree\\topdownshooter\\Assets\\Scripts\\WeaponSystem\\PlayerWeapon.cs" :123 :16) // GlobalDataManager.Instance.uIPlayerSystem.HandleReload(_time) (InvocationExpression)
br ^2

^2: // ExitBlock
return

}
