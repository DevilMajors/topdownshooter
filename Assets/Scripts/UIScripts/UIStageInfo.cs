﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Major.Save;

namespace Major.UI
{
    public class UIStageInfo : MonoBehaviour
    {
        public GameObject levelsPanel;

        private List<Text> completedText = new List<Text>();
        private List<Button> levelsButtons = new List<Button>();
        private int stagesAmount;

        public Hashtable StageTable { get; set; }

        private void Awake()
        {
            OnNewGame();
        }

        private void InitStageTable()
        {
            StageTable = new Hashtable();
            stagesAmount = SceneManager.sceneCountInBuildSettings;
            for (int i = 0; i < stagesAmount - 3; i++)
            {
                StageTable.Add(i, false);
            }
        }

        private void GetStagesTextFields()
        {
            foreach (Text txt in levelsPanel.GetComponentsInChildren<Text>())
            {
                if (txt.gameObject.name.StartsWith("Completed"))
                {
                    completedText.Add(txt);
                }
            }
        }

        private void InitStagesText()
        {
            for (int i = 0; i < StageTable.Count; i++)
            {
                if (StageTable[i].Equals(false))
                {
                    completedText[i].text = "Not completed";
                }
                else
                {
                    completedText[i].text = "completed";
                }
            }
        }

        private void InitStagesButtons()
        {
            foreach (Button bt in levelsPanel.GetComponentsInChildren<Button>())
            {
                levelsButtons.Add(bt);
                bt.interactable = false;
            }

            levelsButtons[0].interactable = true;
            levelsButtons[levelsButtons.Count - 1].interactable = true;
        }

        private void UnlockNextStage(int _stageNumber)
        {
            if (_stageNumber + 1 < stagesAmount && _stageNumber + 1 < 6)
                levelsButtons[_stageNumber + 1].interactable = true;
        }

        public void MarkStageAsCompleted(int _stageNumber)
        {
            _stageNumber -= 3;
            StageTable[_stageNumber] = true;
            levelsButtons[_stageNumber].interactable = false;
            completedText[_stageNumber].text = "Completed";
            UnlockNextStage(_stageNumber);
        }

        public void OnContinue()
        {
            StageTable = SaveLoadSystem.LoadLevelsData().StageTable;
            for (int i = 0; i < StageTable.Count; i++)
            {
                if (StageTable[i].Equals(true))
                {
                    StageTable[i] = true;
                    levelsButtons[i].interactable = false;
                    completedText[i].text = "Completed";
                    UnlockNextStage(i);
                }
            }
        }

        public void OnNewGame()
        {
            InitStageTable();
            GetStagesTextFields();
            InitStagesText();
            InitStagesButtons();
        }
    }
}