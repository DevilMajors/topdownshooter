﻿using Major.EntitySystem;
using UnityEngine;
using UnityEngine.UI;

namespace Major.UI
{
    public class HealthArmorBar : MonoBehaviour
    {
        [SerializeField] private Canvas mainCanvas = null;
        [SerializeField] private Image healthBar = null;
        [SerializeField] private Image armorBar = null;
        [SerializeField] private float displayTime = 3f;

        private void Awake()
        {
            GetComponentInParent<BaseEntityStatsSystem>().OnHealthArmorEntityChanged += HandleHealthArmorChange;
            HideHealthArmorBars();
        }

        private void HandleHealthArmorChange(float _healthPct, float _armorPct)
        {
            healthBar.fillAmount = _healthPct;
            armorBar.fillAmount = _armorPct;
            Invoke(nameof(DisplayHealthArmorBars), 0);
        }

        private void DisplayHealthArmorBars()
        {
            transform.LookAt(Camera.main.transform);
            transform.Rotate(0, 180, 0);
            mainCanvas.gameObject.SetActive(true);
            Invoke(nameof(HideHealthArmorBars), displayTime);
        }

        private void HideHealthArmorBars()
        {
            mainCanvas.gameObject.SetActive(false);
        }
    }
}