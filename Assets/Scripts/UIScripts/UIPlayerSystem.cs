﻿using Major.EntitySystem;
using Major.GameManagers;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Major.UI
{
    public class UIPlayerSystem : MonoBehaviour
    {
        private PlayerEntity playerEntity;
        private PlayerEntityStatsUpgrades playerEntityStatsUpgrades;
        private PointsSystem pointsSystem;

        public Text playerHealthText;
        public Text playerArmorText;
        public Text playerMoneyText;
        public Text upgradeHealth;
        public Text upgradeArmor;
        public Text upgradeDamage;
        public Text upgradeMs;
        public Text playerAmmoText;
        public Text pointsText;
        public Text rampageText;
        public Text enemiesCounter;
        public Image reloadBar;

        private void Start()
        {
            reloadBar.gameObject.SetActive(false);
            playerEntity = GetComponent<PlayerEntity>();
            pointsSystem = GetComponent<PointsSystem>();
            playerEntityStatsUpgrades = GetComponent<PlayerEntityStatsUpgrades>();
            enemiesCounter.text = "Find an exit!";
            OnPlayerStatsUpgrade();
        }

        private void OnPlayerHealthModification()
        {
            playerHealthText.text = "Health: " + playerEntity.EntityCurrentHealth + "/" + playerEntity.EntityMaxHealth;
        }

        private void OnPlayerArmorModification()
        {
            playerArmorText.text = "Armor: " + playerEntity.EntityCurrentArmor + "/" + playerEntity.EntityMaxArmor;
        }

        private void OnPlayerMoneyModification()
        {
            playerMoneyText.text = "Money: " + playerEntity.MoneyAmount;
        }

        private void OnUpgradeHealth()
        {
            upgradeHealth.text = playerEntityStatsUpgrades.CurrentHealthUpgrade + "/5";
        }

        private void OnUpgradeArmor()
        {
            upgradeArmor.text = playerEntityStatsUpgrades.CurrentArmorUpgrade + "/5";
        }

        private void OnUpgradeDamage()
        {
            upgradeDamage.text = playerEntityStatsUpgrades.CurrentDamageUpgrade + "/5";
        }

        private void OnUpgradeMs()
        {
            upgradeMs.text = playerEntityStatsUpgrades.CurrentMovementUpgrade + "/5";
        }

        private void DisplayReloadBar()
        {
            reloadBar.gameObject.SetActive(true);
        }

        private void OnPointsChange()
        {
            pointsText.text = "Points: " + pointsSystem.PlayerPoints;
        }

        private void OnMultiplierChange()
        {
            rampageText.text = "Multiplier: x" + pointsSystem.PointsMultiplier;
        }

        private void HideReloadBar()
        {
            reloadBar.gameObject.SetActive(false);
        }

        private IEnumerator ReloadBarTime(float _time)
        {
            float displayTime = 0;
            while (_time > displayTime)
            {
                displayTime += Time.deltaTime;
                reloadBar.fillAmount = Mathf.Lerp(1, 0, displayTime / _time);
                yield return null;
            }
            reloadBar.fillAmount = 0;
            HideReloadBar();
        }

        public void OnPlayerAmmoModification(int _bulletsLeft, int _magazineSize)
        {
            playerAmmoText.text = "Ammo: " + _bulletsLeft + "/" + _magazineSize;
        }

        public void OnEnemyCounterChange(string _txt)
        {
            enemiesCounter.text = _txt;
        }

        public void HandleReload(float _time)
        {
            DisplayReloadBar();
            StartCoroutine(ReloadBarTime(_time));
        }

        public void OnPointsModification()
        {
            OnPointsChange();
            OnMultiplierChange();
        }

        public void OnPlayerStatsModification()
        {
            OnPlayerHealthModification();
            OnPlayerArmorModification();
            OnPlayerMoneyModification();
        }

        public void OnPlayerStatsUpgrade()
        {
            OnUpgradeArmor();
            OnUpgradeDamage();
            OnUpgradeHealth();
            OnUpgradeMs();
        }
    }
}