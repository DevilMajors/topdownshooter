﻿using UnityEngine;

namespace Major.UI
{
    public class UIScreen : MonoBehaviour
    {
        [SerializeField]private UIScreenType screenType;

        public UIScreenType ScreenType { get => screenType; set => screenType = value; }
    }
}