﻿namespace Major.UI
{
    public enum UIScreenType
    {
        MAIN_MENU,
        EXIT_PROMPT,
        LEVEL_EXIT_PROMPT,
        LEVEL_UI,
        LEVEL_MENU,
        OPTIONS,
        UPGRADE,
        LEVEL_SELECT,
        RESPAWN,
        END
    }
}