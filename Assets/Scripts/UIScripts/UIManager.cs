﻿using UnityEngine;
using System.Collections;
using Major.GameManagers;

namespace Major.UI
{
    public class UIManager : MonoBehaviour
    {
        public GameObject uiSystem;

        private Hashtable uiScreenTable;
        [SerializeField]private UIScreenType previousScreen;
        [SerializeField]private UIScreenType currentScreen;
        [SerializeField]private bool isPauseScreenEnabled = false;

        public static UIManager Instance { get; set; }

        private void Awake()
        {
            if (Instance == null || Instance != this)
            {
                Instance = this;
            }
            GetScreens();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && currentScreen == UIScreenType.LEVEL_UI && !isPauseScreenEnabled)
            {
                ScreenSwitch(UIScreenType.LEVEL_MENU);
                isPauseScreenEnabled = true;
            }
            else if (Input.GetKeyDown(KeyCode.Escape) && isPauseScreenEnabled)
            {
                ScreenSwitch(UIScreenType.LEVEL_UI);
                isPauseScreenEnabled = false;
            }
        }

        public void ScreenEnable(UIScreenType _screenType)
        {
            if (uiScreenTable.ContainsKey(_screenType))
            {
                GameObject screen = (GameObject)uiScreenTable[_screenType];
                screen.SetActive(true);

                previousScreen = currentScreen;
                currentScreen = _screenType;
                CheckScreenToPauseOn();
            }
        }

        public void ScreenEnableByName(string _screenName)
        {
            UIScreenType screenType = (UIScreenType)System.Enum.Parse(typeof(UIScreenType), _screenName);
            if (uiScreenTable.ContainsKey(screenType))
            {
                GameObject screen = (GameObject)uiScreenTable[screenType];
                screen.SetActive(true);

                previousScreen = currentScreen;
                currentScreen = screenType;
                CheckScreenToPauseOn();
            }
        }

        public void ScreenDisable(UIScreenType _screenType)
        {
            if (uiScreenTable.ContainsKey(_screenType))
            {
                GameObject screen = (GameObject)uiScreenTable[_screenType];
                screen.SetActive(false);
            }
        }

        public void ScreenDisableByName(string _screenName)
        {
            UIScreenType screenType = (UIScreenType)System.Enum.Parse(typeof(UIScreenType), _screenName);
            if (uiScreenTable.ContainsKey(screenType))
            {
                GameObject screen = (GameObject)uiScreenTable[screenType];
                screen.SetActive(false);
            }
        }

        public void ScreenSwitch(UIScreenType _screenType)
        {
            ActiveScreensDisable();
            ScreenEnable(_screenType);
        }

        public void ScreenSwitchByName(string _screenName)
        {
            ActiveScreensDisable();
            ScreenEnableByName(_screenName);
        }

        public void ScreenSwitchToPrevious()
        {
            ScreenDisable(currentScreen);
            ScreenEnable(previousScreen);
        }

        public void ActiveScreensDisable()
        {
            foreach (GameObject screen in uiScreenTable.Values)
            {
                if (screen != null && screen.activeSelf)
                {
                    screen.SetActive(false);
                }
            }
        }

        public void SceneChangeFromUI(int _sceneNumber)
        {
            ManagerGame.Instance.LoadSceneByNumber(_sceneNumber);
        }

        public void ApplicationQuitFromUI()
        {
            ManagerGame.Instance.QuitApplication();
        }

        public void ScreenChange()
        {
            InitScreens();
        }

        private UIScreenType GetScreenType(GameObject _screen)
        {
            UIScreen screenType = _screen.GetComponent<UIScreen>();
            if (screenType != null)
            {
                return screenType.ScreenType;
            }
            else
            {
                return UIScreenType.MAIN_MENU;
            }
        }

        private void GetScreens()
        {
            uiScreenTable = new Hashtable();
            if (uiSystem != null)
            {
                foreach (Canvas canvas in uiSystem.GetComponentsInChildren<Canvas>())
                {
                    if (canvas != null && !uiScreenTable.ContainsKey(canvas.gameObject))
                    {
                        uiScreenTable.Add(GetScreenType(canvas.gameObject), canvas.gameObject);
                        canvas.gameObject.SetActive(false);
                    }
                }
                InitScreens();
            }
        }

        private void InitScreens()
        {
            ActiveScreensDisable();
            if (ManagerGame.Instance.CurrentScene == 1)
            {
                currentScreen = UIScreenType.MAIN_MENU;
                ScreenEnable(UIScreenType.MAIN_MENU);
            }
            else
            {
                currentScreen = previousScreen;
                ScreenEnable(UIScreenType.LEVEL_UI);
            }
        }

        private void CheckScreenToPauseOn()
        {
            if (currentScreen != UIScreenType.LEVEL_UI && currentScreen != UIScreenType.MAIN_MENU && currentScreen != UIScreenType.RESPAWN)
            {
                ManagerGame.Instance.PauseGame();
            }
            else
            {
                ManagerGame.Instance.UnpauseGame();
            }
        }

        public void TurnOffMenuOnExit()
        {
            isPauseScreenEnabled = false;
        }
    }
}