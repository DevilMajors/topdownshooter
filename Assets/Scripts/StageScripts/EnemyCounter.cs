﻿using Major.EntitySystem;
using Major.GameManagers;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCounter : MonoBehaviour
{
    [SerializeField] private List<EnemyEntity> enemies = new List<EnemyEntity>();
    [SerializeField] private GameObject stageExit = null;
    private int enemyAmount = 0;
    private bool exitOpen = false;

    public List<EnemyEntity> Enemies { get => enemies; set => enemies = value; }

    private void Awake()
    {
        GlobalDataManager.Instance.EnemyCounter = this;
        enemyAmount = Enemies.Count;
        if(enemyAmount > 0)
        {
            DeactivateExit();
            SetText();
        }
        else
        {
            ActivateExit();
            exitOpen = true;
            SetText();
        }

    }

    private void DeactivateExit()
    {
        stageExit.SetActive(false);
    }

    private void ActivateExit()
    {
        if(enemyAmount <= (Enemies.Count * 2 / 10))
        {
            stageExit.SetActive(true);
            exitOpen = true;
        }
    }

    public void EnemyDeath()
    {
        enemyAmount -= 1;
        ActivateExit();
        SetText();
    }

    private void SetText()
    {
        if (!exitOpen)
        {
            GlobalDataManager.Instance.UIPlayerSystem.OnEnemyCounterChange("Enemies left: " + enemyAmount);
        }
        else
        {
            GlobalDataManager.Instance.UIPlayerSystem.OnEnemyCounterChange("Find an exit!");
        }
    }
}
