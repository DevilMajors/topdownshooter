﻿using Major.EntitySystem;
using Major.GameManagers;
using UnityEngine;

namespace Major.Level.Triggers
{
    public class PickUp : MonoBehaviour
    {
        [SerializeField] private PickupType pickupType = PickupType.MONEY;
        [SerializeField] private int minValue = 10;
        [SerializeField] private int maxValue = 50;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                PlayerEntityStatsSystem entityPlayerSystem = other.GetComponent<PlayerEntityStatsSystem>();
                int amount = RandomizePickUpAmount(minValue, maxValue);

                switch (pickupType)
                {
                    case PickupType.HEALTH:
                        entityPlayerSystem.GiveHealthEntity(amount);
                        break;
                    case PickupType.ARMOR:
                        entityPlayerSystem.GiveArmorEntity(amount);
                        break;
                    case PickupType.MONEY:
                        entityPlayerSystem.GiveMoney(amount);
                        break;
                }
                GlobalDataManager.Instance.DifficultyLevelController.RegisterPositiveImpact(amount);
                Destroy(gameObject);
            }
        }

        private int RandomizePickUpAmount(int min, int max)
        {
            return Random.Range(min, max);
        }
    }

    public enum PickupType
    {
        HEALTH,
        ARMOR,
        MONEY
    }
}