﻿using Major.EntitySystem;
using Major.GameManagers;
using UnityEngine;

namespace Major.GameControllers
{
    [RequireComponent(typeof(Rigidbody))]
    public class ControllerPlayer : MonoBehaviour
    {
        [SerializeField] private float inputVertical;
        [SerializeField] private float inputHorizontal;
        private Rigidbody playerRigidbody;
        private Camera worldCamera;
        private PlayerEntity playerEntity;
        [SerializeField] private Animator playerAnim = null;

        private void Awake()
        {
            playerRigidbody = GetComponent<Rigidbody>();
            playerEntity = GlobalDataManager.Instance.PlayerEntity;
            worldCamera = Camera.main;
        }

        private void FixedUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                Debug.Log(transform.rotation.eulerAngles.y);
            }
            handlePlayer();
        }

        private void handlePlayer()
        {
            if (!playerEntity.IsDead)
            {
                inputHorizontal = Input.GetAxis("Horizontal");
                inputVertical = Input.GetAxis("Vertical");

                handlePlayerMovement();
                handlePlayerRotation();
            }
        }

        private void handlePlayerMovement()
        {
            Vector3 moveVector = new Vector3(inputHorizontal, 0, inputVertical).normalized;
            playerRigidbody.velocity = moveVector * playerEntity.EntityMaxMovementSpeed;
        }

        private void handlePlayerRotation()
        {
            handlePlayerAnimation();
            Ray cameraRay = worldCamera.ScreenPointToRay(Input.mousePosition);
            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            float rayLength;

            if (groundPlane.Raycast(cameraRay, out rayLength))
            {
                Vector3 pointToLook = cameraRay.GetPoint(rayLength);
                playerRigidbody.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
            }
        }

        private void handlePlayerAnimation()
        {
            if (inputHorizontal != 0 || inputVertical != 0)
            {
                playerAnim.SetBool("isWalking", true);
                if((transform.rotation.eulerAngles.y > 135 && transform.rotation.eulerAngles.y < 225 && inputVertical > 0) || 
                    (((transform.rotation.eulerAngles.y > 315 && transform.rotation.eulerAngles.y < 360) || (transform.rotation.eulerAngles.y > 0 && transform.rotation.eulerAngles.y < 45)) && inputVertical < 0) ||
                    (transform.rotation.eulerAngles.y > 45 && transform.rotation.eulerAngles.y < 135 && inputHorizontal < 0) ||
                    (transform.rotation.eulerAngles.y > 225 && transform.rotation.eulerAngles.y < 315 && inputHorizontal > 0))
                {
                    playerAnim.SetBool("isWalkingBackwards", true);
                }
                else
                {
                    playerAnim.SetBool("isWalkingBackwards", false);
                }
            }
            else
            {
                playerAnim.SetBool("isWalking", false);
            }
        }
    }
}