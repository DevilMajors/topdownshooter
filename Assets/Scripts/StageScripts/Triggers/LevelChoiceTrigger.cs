﻿using Major.UI;
using UnityEngine;

namespace Major.Level.Triggers
{
    public class LevelChoiceTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && UIManager.Instance != null)
            {
                UIManager.Instance.ScreenEnableByName("LEVEL_SELECT");
            }
        }
    }
}