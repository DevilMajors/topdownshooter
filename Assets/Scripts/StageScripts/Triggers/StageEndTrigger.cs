﻿using Major.GameManagers;
using UnityEngine;

namespace Major.Level.Triggers
{
    public class StageEndTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && ManagerGame.Instance != null)
            {
                ManagerGame.Instance.StageCompleteTrigger();
                ManagerGame.Instance.OnStageExit();
            }
        }
    }
}