﻿using UnityEngine;

namespace Major.GameControllers
{
    public class ControllerCamera : MonoBehaviour
    {
        private Transform cameraTarget;
        [SerializeField] private float cameraHeight = 15;
        [SerializeField] private float cameraDistance = 5;
        [SerializeField] private float cameraAngle = 20;
        [SerializeField] private float cameraSmoothTime = 0.5f;

        private Vector3 refVector;

        private void Start()
        {
            if (cameraTarget != null)
            {
                CameraAction();
            }
            else
            {
                cameraTarget = FindObjectOfType<ControllerPlayer>().gameObject.transform;
                CameraAction();
            }
        }
        private void FixedUpdate()
        {
            if (cameraTarget)
            {
                CameraAction();
            }
        }

        private void CameraAction()
        {
            Vector3 worldPosition = Vector3.forward * -cameraDistance + Vector3.up * cameraHeight;
            Vector3 rotatedVector = Quaternion.AngleAxis(cameraAngle, Vector3.up) * worldPosition;
            Vector3 flatTargetPosition = cameraTarget.position;
            flatTargetPosition.y = 0f;

            Vector3 finalPosition = flatTargetPosition + rotatedVector;
            transform.position = Vector3.SmoothDamp(transform.position, finalPosition, ref refVector, cameraSmoothTime);
            transform.LookAt(flatTargetPosition);
        }
    }
}