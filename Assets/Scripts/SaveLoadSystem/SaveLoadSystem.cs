﻿using Major.GameManagers;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Major.Save
{
    public static class SaveLoadSystem
    {
        private static string directoryPath = Path.Combine(Application.persistentDataPath, "saves");

        public static void CreateFolderIfNotExist()
        {
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }
        }

        public static void SavePlayer()
        {
            CreateFolderIfNotExist();
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Path.Combine(directoryPath, "playerInfo.save");
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData data = new PlayerData(GlobalDataManager.Instance.PlayerEntityStatsUpgrades, GlobalDataManager.Instance.PlayerEntity);

            formatter.Serialize(stream, data);
            stream.Close();
        }

        public static void SaveStageInfo()
        {
            CreateFolderIfNotExist();
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Path.Combine(directoryPath, "stageInfo.save");
            FileStream stream = new FileStream(path, FileMode.Create);

            LevelsData data = new LevelsData(GlobalDataManager.Instance.UIStageInfo.StageTable);

            formatter.Serialize(stream, data);
            stream.Close();
        }

        public static PlayerData LoadPlayer()
        {
            string path = Path.Combine(directoryPath, "playerInfo.save");
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                PlayerData data = formatter.Deserialize(stream) as PlayerData;
                stream.Close();

                return data;
            }
            else
            {
                Debug.LogError("Save file not found");
                return null;
            }
        }

        public static LevelsData LoadLevelsData()
        {
            string path = Path.Combine(directoryPath, "stageInfo.save");
            if (File.Exists(path))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);
                LevelsData data = formatter.Deserialize(stream) as LevelsData;
                stream.Close();

                return data;
            }
            else
            {
                Debug.LogError("Save file not found");
                return null;
            }
        }

        public static void LoadAllData()
        {
            LoadLevelsData();
            LoadPlayer();
        }

        public static bool CheckForSaveState()
        {
            if (File.Exists(Path.Combine(directoryPath, "playerInfo.save")) && File.Exists(Path.Combine(directoryPath, "stageInfo.save")))
            {
                return true;
            }
            return false;
        }
    }
}