﻿using System.Collections;

namespace Major.Save
{
    [System.Serializable]
    public class LevelsData
    {
        private Hashtable stageTable;
        public Hashtable StageTable { get => stageTable; set => stageTable = value; }

        public LevelsData(Hashtable _stageTable)
        {
            StageTable = _stageTable;
        }
    }
}