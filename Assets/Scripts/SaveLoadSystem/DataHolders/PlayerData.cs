﻿using Major.EntitySystem;
using Major.GameManagers;

namespace Major.Save
{
    [System.Serializable]
    public class PlayerData
    {
        private int playerHealthUpgrade;
        private int playerArmorUpgrade;
        private int playerMovementSpeedUpgrade;
        private int playerDamageUpgrade;
        private int playerMoney;
        private int playerPoints;

        public int PlayerHealthUpgrade { get => playerHealthUpgrade; set => playerHealthUpgrade = value; }
        public int PlayerArmorUpgrade { get => playerArmorUpgrade; set => playerArmorUpgrade = value; }
        public int PlayerMovementSpeedUpgrade { get => playerMovementSpeedUpgrade; set => playerMovementSpeedUpgrade = value; }
        public int PlayerDamageUpgrade { get => playerDamageUpgrade; set => playerDamageUpgrade = value; }
        public int PlayerMoney { get => playerMoney; set => playerMoney = value; }
        public int PlayerPoints { get => playerPoints; set => playerPoints = value; }

        public PlayerData(PlayerEntityStatsUpgrades _playerEntityStatsUpgrades, PlayerEntity _playerEntity)
        {
            PlayerHealthUpgrade = _playerEntityStatsUpgrades.CurrentHealthUpgrade;
            PlayerArmorUpgrade = _playerEntityStatsUpgrades.CurrentArmorUpgrade;
            PlayerMovementSpeedUpgrade = _playerEntityStatsUpgrades.CurrentMovementUpgrade;
            PlayerDamageUpgrade = _playerEntityStatsUpgrades.CurrentDamageUpgrade;
            PlayerMoney = _playerEntity.MoneyAmount;
            PlayerPoints = GlobalDataManager.Instance.PointsSystem.PlayerPoints;
        }
    }
}