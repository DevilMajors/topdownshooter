﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Major.Save
{
    public class LoadController : MonoBehaviour
    {
        [SerializeField] private Button continueButton = null;

        private void Awake()
        {
            SetContinueButton();
        }

        private void SetContinueButton()
        {
            if (!SaveLoadSystem.CheckForSaveState())
            {
                continueButton.interactable = false;
            }
            else
            {
                continueButton.interactable = true;
            }
        }
    }
}