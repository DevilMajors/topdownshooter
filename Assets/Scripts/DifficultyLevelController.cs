﻿using Major.EntitySystem;
using Major.GameManagers;
using Major.Timer;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Major.GameControllers
{
    public class DifficultyLevelController : MonoBehaviour
    {
        [SerializeField] private List<EnemyEntity> enemyBaseEntities = new List<EnemyEntity>();
        private readonly Dictionary<EnemyType, int> registeredImpactAmount = new Dictionary<EnemyType, int>();
        private readonly ActionTimer difficultyCheckTimer = new ActionTimer(30);
        public float PositiveImpactAmount { get; set; }
        public float NegativeImpactAmount { get; set; }
        public List<EnemyEntity> EnemyBaseEntities { get => enemyBaseEntities; set => enemyBaseEntities = value; }

        private void Awake()
        {
            InitEnemyDictionary();
        }

        private void Update()
        {
            if (GlobalDataManager.Instance.ManagerGame.CurrentScene > 2 && !GlobalDataManager.Instance.ManagerGame.gameIsPaused)
            {
                if (difficultyCheckTimer.TimerCycle())
                {
                    CheckImpact();
                    PositiveImpactAmount += 0.25f;
                }
            }
        }

        public void RegisterPositiveImpact(float _impactAmount)
        {
            if(_impactAmount >= 35)
            {
                PositiveImpactAmount += 1.25f;
            }else if(_impactAmount < 35 && _impactAmount >= 25)
            {
                PositiveImpactAmount += 0.75f;
            }else
            {
                PositiveImpactAmount += 0.25f;
            }
        }

        public void RegisterNegativeImpact(float _impactAmount, EnemyEntity _enemy)
        {
            RegisterEnemyType(_enemy.EnemyType);
            if(_impactAmount > 20)
            {
                NegativeImpactAmount += 1.25f;
            }
            else
            {
                NegativeImpactAmount += 0.75f;
            }
        }

        public void CheckImpact()
        {
            if(Mathf.Abs(PositiveImpactAmount - NegativeImpactAmount) > 3)
            {
                if(NegativeImpactAmount > PositiveImpactAmount)
                {
                    LowerDifficulty();
                    ResetImpact();
                }
                else if(NegativeImpactAmount < PositiveImpactAmount)
                {
                    HigherDifficulty();
                    ResetImpact();
                }
            }
        }

        private void LowerDifficulty()
        {
            float modifierPercent = NegativeImpactAmount;
            List<EnemyType> enemiesWithBiggestImpact = GetEnemiesWithBiggestImpact();

            foreach (var enemy in enemiesWithBiggestImpact)
            {
                ChangeBaseByModifierAmount(enemy, modifierPercent);
                EnemyEntity baseEntity = GetEntityOfBaseType(enemy);
                List<EnemyEntity> enemiesToModify = GetExistingEnemiesOfType(enemy);

                if (baseEntity != null && enemiesToModify.Count != 0)
                {
                    foreach (var enemyMod in enemiesToModify)
                    {
                        SwapStatsToBase(enemyMod, baseEntity);
                    }
                }
                ResetEnemyImpact(enemiesWithBiggestImpact);
            }
        }

        private void HigherDifficulty()
        {
            float modifierPercent = -PositiveImpactAmount;
            List<EnemyType> enemiesWithLowestImpact = GetEnemiesWithLowestImpact();
            List<EnemyEntity> enemiesToModify = GetExistingEnemiesOfMultipleTypes(enemiesWithLowestImpact);

            for (int i = 0; i < enemiesWithLowestImpact.Count; i++)
            {
                ChangeBaseByModifierAmount(enemiesWithLowestImpact[i], modifierPercent);
            }

            foreach (var enemy in enemiesToModify)
            {
                SwapStatsToBase(enemy, GetEntityOfBaseType(enemy.EnemyType));
            }
            ResetEnemyImpact(enemiesWithLowestImpact);
        }

        private void ResetImpact()
        {
            PositiveImpactAmount = 0;
            NegativeImpactAmount = 0;
        }

        private List<EnemyType> GetEnemiesWithBiggestImpact()
        {
            List<EnemyType> enemiesWithHighestImpact = new List<EnemyType>();
            List<EnemyType> controllList = GetExistingEnemiesTypes();
            foreach (var enemy in registeredImpactAmount)
            {
                if (enemy.Value >= 2 && controllList.Contains(enemy.Key))
                {
                    enemiesWithHighestImpact.Add(enemy.Key);
                }
            }
            return enemiesWithHighestImpact;
        }

        private List<EnemyType> GetEnemiesWithLowestImpact()
        {
            List<EnemyType> enemiesWithLowestImpact = new List<EnemyType>();
            List<EnemyType> controllList = GetExistingEnemiesTypes();
            foreach (var enemy in registeredImpactAmount)
            {
                if(enemy.Value < 2 && controllList.Contains(enemy.Key))
                {
                    enemiesWithLowestImpact.Add(enemy.Key);
                }
            }
            return enemiesWithLowestImpact;
        }

        private void ResetEnemyImpact(List<EnemyType> enemyList)
        {
            if(enemyList != null)
            {
                foreach (EnemyType enemy in enemyList)
                {
                    registeredImpactAmount[enemy] = 0;
                }
            }
        }

        private void RegisterEnemyType(EnemyType _enemyType)
        {
            if (registeredImpactAmount.ContainsKey(_enemyType))
            {
                registeredImpactAmount[_enemyType] += 1;
            }
            else
            {
                registeredImpactAmount.Add(_enemyType, 1);
            }
        }

        private List<EnemyEntity> GetExistingEnemies()
        {
            List<EnemyEntity> enemies = new List<EnemyEntity>();
            foreach (var enemy in FindObjectsOfType<EnemyEntity>())
            {
                if (enemy.gameObject.CompareTag("Enemy"))
                {
                    enemies.Add(enemy);
                }
            }
            return enemies;
        }

        private List<EnemyType> GetExistingEnemiesTypes()
        {
            List<EnemyType> enemies = new List<EnemyType>();
            foreach (var enemy in FindObjectsOfType<EnemyEntity>())
            {
                if (enemy.gameObject.CompareTag("Enemy") && !enemies.Contains(enemy.EnemyType))
                {
                    enemies.Add(enemy.EnemyType);
                }
            }
            return enemies;
        }

        private List<EnemyEntity> GetExistingEnemiesOfType(EnemyType _enemyType)
        {
            List<EnemyEntity> enemiesOfType = new List<EnemyEntity>();
            List<EnemyEntity> enemiesAll = GetExistingEnemies();

            if(enemiesAll.Count > 0)
            {
                foreach (var enemy in enemiesAll)
                {
                    if (enemy.EnemyType.Equals(_enemyType))
                    {
                        enemiesOfType.Add(enemy);
                    }
                }
            }
            return enemiesOfType;
        }

        private List<EnemyEntity> GetExistingEnemiesOfMultipleTypes(List<EnemyType> _enemiesTypes)
        {
            List<EnemyEntity> enemies = new List<EnemyEntity>();
            List<EnemyEntity> enemiesAll = GetExistingEnemies();

            for (int i = 0; i < _enemiesTypes.Count; i++)
            {
                if (enemiesAll.Count > 0)
                {
                    foreach (var enemy in enemiesAll)
                    {
                        if (enemy.EnemyType.Equals(_enemiesTypes[i]))
                        {
                            enemies.Add(enemy);
                        }
                    }
                }
            }
            return enemies;
        }

        private EnemyEntity GetEntityOfBaseType(EnemyType _enemyType)
        {
            foreach (var entity in EnemyBaseEntities)
            {
                if (entity.EnemyType == _enemyType)
                {
                    return entity;
                }
            }
            return null;
        }

        private bool IsEnemyDamaged(EnemyEntity _enemy)
        {
            if (_enemy.EntityCurrentHealth != _enemy.EntityMaxHealth && _enemy.EntityCurrentArmor != _enemy.EntityMaxArmor)
            {
                return true;
            }
            return false;
        }

        private void ChangeStatsPositive(EnemyEntity _baseEntity, float _modifierAmount)
        {
            if(_baseEntity.EntityMaxHealth < 200)
            {
                _baseEntity.EntityMaxHealth -= (int)(_baseEntity.EntityMaxHealth * (_modifierAmount / 100));
                _baseEntity.EntityCurrentHealth = _baseEntity.EntityMaxHealth;
            }
            if(_baseEntity.EntityMaxArmor < 200)
            {
                _baseEntity.EntityMaxArmor -= (int)(_baseEntity.EntityMaxArmor * (_modifierAmount / 100));
                _baseEntity.EntityCurrentArmor = _baseEntity.EntityMaxArmor;
            }
            if(_baseEntity.AttackDamage < 75)
            {
                _baseEntity.AttackDamage -= (int)(_baseEntity.AttackDamage * (_modifierAmount / 100));
            }
            if(_baseEntity.EntityMaxMovementSpeed < 5)
            {
                _baseEntity.EntityMaxMovementSpeed -= (_baseEntity.EntityMaxMovementSpeed * (_modifierAmount / 100));
            }
        }

        private void ChangeStatsNegative(EnemyEntity _baseEntity, float _modifierAmount)
        {
            if (_baseEntity.EntityMaxHealth > 25)
            {
                _baseEntity.EntityMaxHealth -= (int)(_baseEntity.EntityMaxHealth * (_modifierAmount / 100));
                _baseEntity.EntityCurrentHealth = _baseEntity.EntityMaxHealth;
            }
            if (_baseEntity.EntityMaxArmor > 25)
            {
                _baseEntity.EntityMaxArmor -= (int)(_baseEntity.EntityMaxArmor * (_modifierAmount / 100));
                _baseEntity.EntityCurrentArmor = _baseEntity.EntityMaxArmor;
            }
            if (_baseEntity.AttackDamage > 10)
            {
                _baseEntity.AttackDamage -= (int)(_baseEntity.AttackDamage * (_modifierAmount / 100));
            }
            if (_baseEntity.EntityMaxMovementSpeed > 2)
            {
                _baseEntity.EntityMaxMovementSpeed -= (_baseEntity.EntityMaxMovementSpeed * (_modifierAmount / 100));
            }
        }

        private void ChangeBaseByModifierAmount(EnemyType _enemyToChange, float _modifierAmount)
        {
            if(_modifierAmount > 0)
            {
                ChangeStatsNegative(GetEntityOfBaseType(_enemyToChange), _modifierAmount);
            }
            else
            {
                ChangeStatsPositive(GetEntityOfBaseType(_enemyToChange), _modifierAmount);
            }
        }

        private void SwapStatsToBase(EnemyEntity _enemyTo, EnemyEntity _enemyFrom)
        {
            if (!IsEnemyDamaged(_enemyTo))
            {
                _enemyTo.EntityCurrentHealth = _enemyFrom.EntityCurrentHealth;
                _enemyTo.EntityMaxHealth = _enemyFrom.EntityMaxHealth;
                _enemyTo.EntityCurrentArmor = _enemyFrom.EntityCurrentArmor;
                _enemyTo.EntityMaxArmor = _enemyFrom.EntityMaxArmor;
                _enemyTo.EntityMaxMovementSpeed = _enemyFrom.EntityMaxMovementSpeed;
                _enemyTo.AttackDamage = _enemyFrom.AttackDamage;
                _enemyTo.AttackDelay = _enemyFrom.AttackDelay;
                _enemyTo.AttackRange = _enemyFrom.AttackRange;
                _enemyTo.StoppingDistance = _enemyFrom.StoppingDistance;
                _enemyTo.DetectionRange = _enemyFrom.DetectionRange;
                _enemyTo.gameObject.GetComponent<Enemy>().NavAgent.speed = _enemyFrom.EntityMaxMovementSpeed;
            }
        }

        private void InitEnemyDictionary()
        {
            registeredImpactAmount.Add(EnemyType.MELE, 0);
            registeredImpactAmount.Add(EnemyType.RANGE_PISTOL, 0);
            registeredImpactAmount.Add(EnemyType.RANGE_RIFLE, 0);
            registeredImpactAmount.Add(EnemyType.RANGE_SHOTGUN, 0);
            registeredImpactAmount.Add(EnemyType.CHARGER, 0);
            registeredImpactAmount.Add(EnemyType.EXPLODING, 0);
        }

        public void SetEnemiesToBase()
        {
            List<EnemyEntity> tmpEntityTable = GetExistingEnemies();

            if (tmpEntityTable.Count != 0 && EnemyBaseEntities != null)
            {
                for (int i = 0; i < tmpEntityTable.Count; i++)
                {
                    EnemyEntity baseEntity = GetEntityOfBaseType(tmpEntityTable[i].EnemyType);
                    if (baseEntity != null)
                    {
                        SwapStatsToBase(tmpEntityTable[i], baseEntity);
                    }
                }
            }
        }
    }
}