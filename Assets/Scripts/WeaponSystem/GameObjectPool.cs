﻿using System.Collections.Generic;
using UnityEngine;

namespace Major.GameControllers
{
    public class GameObjectPool : MonoBehaviour
    {
        private readonly Queue<GameObject> objects = new Queue<GameObject>();
        public static GameObjectPool Instance { get; set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public GameObject GetPooledObj(GameObject _bullet)
        {
            if (objects.Count <= 0)
            {
                AddGameObject(1, _bullet);
            }
            return objects.Dequeue();
        }

        public void ReturnToPool(GameObject _objectToReturn)
        {
            _objectToReturn.SetActive(false);
            objects.Enqueue(_objectToReturn);
        }

        public void AddGameObject(int _number, GameObject _bullet)
        {
            for(int i = 0; i < _number; i++)
            {
                var newGameObject = Instantiate(_bullet);
                newGameObject.SetActive(false);
                objects.Enqueue(newGameObject);
            }
        }
    }
}