﻿using Major.EntitySystem;
using UnityEngine;

namespace Major.WeaponSystem
{
    public class EnemyWeaponController : MonoBehaviour
    {
        [SerializeField] private GameObject enemyWeaponGameObject = null;
        private EnemyEntity enemyEntity;

        public EnemyWeapon EnemyWeapon { get; set; }

        private void Awake()
        {
            if(enemyEntity == null)
            {
                enemyEntity = GetComponent<EnemyEntity>();
                EnemyWeapon = enemyWeaponGameObject.GetComponent<EnemyWeapon>();

                EnemyWeapon.GunDamage = (int)enemyEntity.AttackDamage;
                EnemyWeapon.Range = enemyEntity.AttackRange  * 1.5f;
                EnemyWeapon.WeaponInit();
            }
        }
    }
}