﻿using Major.GameControllers;
using UnityEngine;

namespace Major.WeaponSystem
{
    public class EnemyWeapon : MonoBehaviour
    {
        [SerializeField] private string weaponName;
        [SerializeField] private float spreadAngle = 0, bulletSpeed = 3;
        [SerializeField] private int magazineSize = 10, bulletsPerTap = 1;
        private int bulletsLeft;

        [SerializeField] private GameObject bulletPrefab = null;
        [SerializeField] private Transform shootingPoint = null;

        public int GunDamage { get; set; }
        public float Range { get; set; }

        public void ShootWeapon()
        {
            if(bulletsLeft >= bulletsPerTap)
            {
                ShootSeries();
            }

            if (!HaveBulletsInMagazine())
            {
                ReloadWeapon();
            }
        }

        public void WeaponInit()
        {
            bulletsLeft = magazineSize;
        }

        private void ReloadWeapon()
        {
            bulletsLeft = magazineSize;
        }

        private bool HaveBulletsInMagazine()
        {
            return bulletsLeft > 0;
        }

        private void ShootSeries()
        {
            for (int i = 0; i < bulletsPerTap; i++)
            {
                float tmpSpreadAngle = Random.Range(-spreadAngle, spreadAngle);
                GameObject bullet = GameObjectPool.Instance.GetPooledObj(bulletPrefab);
                bullet.transform.position = shootingPoint.position;
                bullet.transform.rotation = shootingPoint.rotation * Quaternion.AngleAxis(tmpSpreadAngle, Vector3.up);

                Bullet bull = bullet.GetComponent<Bullet>();
                bull.bulletDamage = GunDamage;
                bull.bulletSpeed = bulletSpeed;
                bull.rangeOfLife = Range;
                bull.shootBy = gameObject.transform.parent.gameObject;

                bullet.SetActive(true);
                bulletsLeft--;
            }
        }
    }
}