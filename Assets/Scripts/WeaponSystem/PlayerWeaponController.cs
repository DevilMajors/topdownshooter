﻿using Major.EntitySystem;
using Major.GameManagers;
using System.Collections.Generic;
using UnityEngine;

namespace Major.WeaponSystem
{
    public class PlayerWeaponController : MonoBehaviour
    {
        public List<GameObject> playerWeapons = new List<GameObject>();

        private PlayerEntity playerEntity;
        private PlayerWeapon currentWeapon;
        private bool isWeaponEnabled = true;
        [SerializeField] private Animator playerAnim = null;
        private void Awake()
        {
            playerEntity = GlobalDataManager.Instance.PlayerEntity;
            foreach (GameObject gameObject in playerWeapons)
            {
                gameObject.SetActive(false);
                gameObject.GetComponent<PlayerWeapon>().WeaponInit();
            }
            EquipWeapon(0);
        }

        public void Update()
        {
            if (playerEntity.IsDead && isWeaponEnabled)
            {
                currentWeapon.gameObject.SetActive(false);
                isWeaponEnabled = false;
            }else if(!isWeaponEnabled && !playerEntity.IsDead)
            {
                currentWeapon.gameObject.SetActive(true);
                isWeaponEnabled = true;
            }

            if (!playerEntity.IsDead && !ManagerGame.Instance.gameIsPaused)
            {
                currentWeapon.PlayerInput();

                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    EquipWeapon(0);
                }
                else if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    EquipWeapon(1);
                }
                else if (Input.GetKeyDown(KeyCode.Alpha3))
                {
                    EquipWeapon(2);
                }
            }
        }

        private void EquipWeapon(int _weaponNumber)
        {
            if (_weaponNumber < playerWeapons.Count && currentWeapon != playerWeapons[_weaponNumber])
            {
                UnequipWeapons();
                playerWeapons[_weaponNumber].SetActive(true);
                currentWeapon = playerWeapons[_weaponNumber].GetComponent<PlayerWeapon>();
                if(playerAnim != null)
                {
                    playerAnim.SetInteger("weaponType", _weaponNumber);
                }
            }
        }

        private void UnequipWeapons()
        {
            foreach (GameObject gameObject in playerWeapons)
            {
                gameObject.SetActive(false);
            }
        }
    }
}