﻿using Major.GameControllers;
using Major.GameManagers;
using UnityEngine;

namespace Major.WeaponSystem
{
    public class PlayerWeapon : MonoBehaviour
    {
        [SerializeField] private string weaponName;
        public int gunDamage;
        public float timeBetweenSeries, spreadAngle, range, reloadTime, timeBetweenShoot, bulletSpeed;
        public int magazineSize, bulletsPerTap;
        public bool allowButtonHold;
        private int bulletsLeft, bulletsShot;
        private bool isShooting, isReadyToShoot, isReloading, haveBullets;

        public GameObject bulletPrefab;
        public Transform shootingPoint;
        public GameObject muzzleFlash, bulletHole;


        private void OnEnable()
        {
            SetBulletsOnUI();
        }

        private void Awake()
        {
            WeaponInit();
            bulletsLeft = magazineSize;
            isReadyToShoot = true;
        }

        private void ShootWeapon()
        {
            isReadyToShoot = false;

            float tmpSpreadAngle = Random.Range(-spreadAngle, spreadAngle);
            GameObject bullet = GameObjectPool.Instance.GetPooledObj(bulletPrefab);
            bullet.transform.position = shootingPoint.position;
            bullet.transform.rotation = shootingPoint.rotation * Quaternion.AngleAxis(tmpSpreadAngle, Vector3.up);

            Bullet bull = bullet.GetComponent<Bullet>();
            bull.bulletDamage = gunDamage * GlobalDataManager.Instance.PlayerEntity.MaxDamageModifier;
            bull.bulletSpeed = bulletSpeed;
            bull.rangeOfLife = range;
            bull.shootBy = gameObject.transform.parent.gameObject;

            bullet.SetActive(true);

            bulletsLeft--;
            bulletsShot--;
            SetBulletsOnUI();

            Invoke(nameof(ResetShootWeapon), timeBetweenSeries);
            if (HaveBulletsInMagazine() && bulletsShot > 0)
            {
                Invoke(nameof(ShootWeapon), timeBetweenShoot);
            }
        }

        private void ReloadWeapon()
        {
            isReloading = true;
            Invoke(nameof(ReloadFinish), reloadTime);
        }

        private void ReloadFinish()
        {
            bulletsLeft = magazineSize;
            isReloading = false;
            SetBulletsOnUI();
        }

        private void ResetShootWeapon()
        {
            isReadyToShoot = true;
        }

        private bool HaveBulletsInMagazine()
        {
            return bulletsLeft > 0;
        }

        private void SetBulletsOnUI()
        {
            GlobalDataManager.Instance.UIPlayerSystem.OnPlayerAmmoModification(bulletsLeft, magazineSize);
        }

        public void WeaponInit()
        {
            bulletsLeft = magazineSize;
            isReadyToShoot = true;
        }

        public void PlayerInput()
        {
            if (allowButtonHold)
            {
                isShooting = Input.GetKey(KeyCode.Mouse0);
            }
            else
            {
                isShooting = Input.GetKeyDown(KeyCode.Mouse0);
            }

            if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !isReloading)
            {
                PassDataToReloadBar(reloadTime);
                ReloadWeapon();
            }

            if (isReadyToShoot && isShooting && !isReloading && HaveBulletsInMagazine())
            {
                bulletsShot = bulletsPerTap;
                ShootWeapon();
            }
        }

        private void PassDataToReloadBar(float _time)
        {
            if (transform.parent.gameObject.CompareTag("Player"))
            {
                GlobalDataManager.Instance.UIPlayerSystem.HandleReload(_time);
            }
        }
    }
}