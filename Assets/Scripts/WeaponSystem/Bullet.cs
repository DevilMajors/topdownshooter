﻿using Major.EntitySystem;
using Major.GameControllers;
using Major.GameManagers;
using UnityEngine;

namespace Major.WeaponSystem
{
    public class Bullet : MonoBehaviour
    {
        private float bulletMaxLifeTime = 5f;
        private float bulletCurrentLifeTime;
        private Vector3 distanceCovered;

        public float bulletSpeed = 5f;
        public float bulletDamage = 10;
        public float rangeOfLife;
        public GameObject shootBy;

        private void OnEnable()
        {
            bulletCurrentLifeTime = 0f;
            distanceCovered = new Vector3(0, 0, 0);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Stage"))
            {
                GameObjectPool.Instance.ReturnToPool(gameObject);
            }
            if(shootBy != null)
            {
                if (shootBy.CompareTag("Player") && other.CompareTag("Enemy"))
                {
                    if (other.gameObject.GetComponent<EnemyEntityStatsSystem>() != null)
                    {
                        EnemyEntityStatsSystem baseEntityStatsSystem = other.gameObject.GetComponent<EnemyEntityStatsSystem>();
                        baseEntityStatsSystem.DamageEntity(bulletDamage);
                    }
                    GameObjectPool.Instance.ReturnToPool(gameObject);
                }
                else if (shootBy.CompareTag("Enemy") && other.CompareTag("Player"))
                {
                    if (other.gameObject.GetComponent<PlayerEntityStatsSystem>() != null)
                    {
                        PlayerEntityStatsSystem baseEntityStatsSystem = other.gameObject.GetComponent<PlayerEntityStatsSystem>();
                        baseEntityStatsSystem.DamageEntity(bulletDamage, shootBy.GetComponent<EnemyEntity>());
                    }
                    GameObjectPool.Instance.ReturnToPool(gameObject);
                }
            }
        }

        private void BulletTimeExpire()
        {
            if (bulletCurrentLifeTime >= bulletMaxLifeTime)
            {
                GameObjectPool.Instance.ReturnToPool(gameObject);
            }
            else
            {
                distanceCovered += Vector3.forward * Time.deltaTime * bulletSpeed;
                transform.Translate(Vector3.forward * Time.deltaTime * bulletSpeed);

                if (distanceCovered.magnitude >= rangeOfLife)
                {
                    GameObjectPool.Instance.ReturnToPool(gameObject);
                }
            }
        }

        private void Update()
        {
            if (!ManagerGame.Instance.gameIsPaused)
            {
                bulletCurrentLifeTime += Time.deltaTime;
                BulletTimeExpire();
            }
        }
    }
}