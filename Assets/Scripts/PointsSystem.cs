﻿using Major.GameManagers;
using Major.Save;
using Major.Timer;
using Major.UI;
using UnityEngine;

public class PointsSystem : MonoBehaviour
{
    private UIPlayerSystem uIPlayerSystem; 
    private readonly ActionTimer rampageTime = new ActionTimer(7);
    private bool rampageStarted = false;

    public int PlayerPoints { get; set; }
    public int PointsMultiplier { get; set; } = 1;

    private void Awake()
    {
        uIPlayerSystem = GetComponent<UIPlayerSystem>();
    }

    private void Update()
    {
        if (rampageStarted && GlobalDataManager.Instance.ManagerGame.CurrentScene > 2)
        {
            if (rampageTime.TimerCycle())
            {
                ResetMultiplier();
                ResetRampage();
            }
        }
    }

    private void ResetMultiplier()
    {
        PointsMultiplier = 1;
        uIPlayerSystem.OnPointsModification();
    }

    private void ResetRampage()
    {
        rampageStarted = false;
    }

    private void IncreaseMultiplier()
    {
        PointsMultiplier += 1;
    }

    public void CountPoints(int _points) {
        if(!rampageStarted)
        {
            rampageStarted = true;
        }
        PlayerPoints += _points * PointsMultiplier;
        IncreaseMultiplier();
        rampageTime.RestartTimer();
        uIPlayerSystem.OnPointsModification();
    }

    public void OnNewGame()
    {
        PlayerPoints = 0;
        uIPlayerSystem.OnPointsModification();
    }

    public void OnContinue()
    {
        PlayerData playerData = SaveLoadSystem.LoadPlayer();

        if(playerData != null)
        {
            PlayerPoints = playerData.PlayerPoints;
        }
        uIPlayerSystem.OnPointsModification();
    }
}
