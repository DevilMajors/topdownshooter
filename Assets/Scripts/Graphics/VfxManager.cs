﻿using System.Collections.Generic;
using UnityEngine;

namespace Major.GameManagers.VFXManager
{
    public class VfxManager : MonoBehaviour
    {
        private static VfxManager instance;
        [SerializeField] private List<VfxEffect> effectsList = new List<VfxEffect>();

        public static VfxManager Instance { get => instance; private set => instance = value; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public void PlayVFX(string _vfxName, float _playTime, Transform _vfxPlace)
        {
            int VfxNumber = GetVFXNumber(_vfxName);
            if (VfxNumber != -1)
            {
                GameObject tempEffect = Instantiate(effectsList[VfxNumber].VfxGameObject, _vfxPlace.position, Quaternion.identity);
                Destroy(tempEffect, _playTime);
            }
        }

        private int GetVFXNumber(string _vfxName)
        {
            int vfxNumber = 0;
            foreach (var effect in effectsList)
            {
                if (effect.VfxName.ToUpper().Equals(_vfxName.ToUpper()))
                {
                    return vfxNumber;
                }
                vfxNumber++;
            }
            return -1;
        }
    }
}