﻿using System;
using UnityEngine;

namespace Major.GameManagers.VFXManager
{
    [Serializable]
    public struct VfxEffect
    {
        [SerializeField]private string vfxName;
        [SerializeField]private GameObject vfxGameObject;

        public string VfxName { get => vfxName; set => vfxName = value; }
        public GameObject VfxGameObject { get => vfxGameObject; set => vfxGameObject = value; }
    }
}