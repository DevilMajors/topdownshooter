﻿using Major.EntitySystem;
using Major.GameControllers;
using Major.UI;
using UnityEngine;

namespace Major.GameManagers
{
    public class GlobalDataManager : MonoBehaviour
    {
        public static GlobalDataManager Instance { get; set; }
        public UIPlayerSystem UIPlayerSystem { get; set; }
        public PlayerStatsManager PlayerStats { get; set; }
        public ManagerGame ManagerGame { get; set; }
        public UIManager UIManager { get; set; }
        public UIStageInfo UIStageInfo { get; set; }
        public PlayerEntity PlayerEntity { get; set; }
        public PlayerEntityStatsUpgrades PlayerEntityStatsUpgrades { get; set; }
        public RespawnController RespawnController { get; set; }
        public DifficultyLevelController DifficultyLevelController { get; set; }
        public PointsSystem PointsSystem { get; set; }
        public EnemyCounter EnemyCounter { get; set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            ManagerGame = GetComponent<ManagerGame>();
            UIPlayerSystem = GetComponent<UIPlayerSystem>();
            PlayerStats = GetComponent<PlayerStatsManager>();
            UIManager = GetComponent<UIManager>();
            RespawnController = GetComponent<RespawnController>();
            DifficultyLevelController = GetComponent<DifficultyLevelController>();
            PlayerEntityStatsUpgrades = GetComponent<PlayerEntityStatsUpgrades>();
            PlayerEntity = GetComponent<PlayerEntity>();
            UIStageInfo = GetComponent<UIStageInfo>();
            PointsSystem = GetComponent<PointsSystem>();
        }

        private void Start()
        {
            ManagerGame.OnStageEntered += PlayerStats.SetPlayerStatsToMaxOnLevelEnter;
            ManagerGame.OnStageEntered += UIPlayerSystem.OnPlayerStatsModification;
            ManagerGame.OnSceneLoaded += DifficultyLevelController.SetEnemiesToBase;
            PlayerStats.OnStatsUpgrade += UIPlayerSystem.OnPlayerStatsUpgrade;
            PlayerStats.OnStatsUpgrade += UIPlayerSystem.OnPlayerStatsModification;
            RespawnController.OnPlayerRespawn += UIPlayerSystem.OnPlayerStatsModification;

            ManagerGame.OnGameLoad += UIStageInfo.OnContinue;
            ManagerGame.OnGameLoad += PlayerStats.OnContinue;
            ManagerGame.OnGameLoad += PointsSystem.OnContinue;
            ManagerGame.OnNewGame += UIStageInfo.OnNewGame;
            ManagerGame.OnNewGame += PlayerStats.OnNewGame;
            ManagerGame.OnNewGame += PointsSystem.OnNewGame;
        }
    }
}