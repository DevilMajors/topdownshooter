﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    public AudioMixer masterVolume;
    public Dropdown resolutionDropdown;
    public Dropdown qualityDropdown;
    public Toggle toggle;
    private Resolution[] resolutions;

    public void SetMasterVolume(float _value)
    {
        masterVolume.SetFloat("MasterVolume", _value);
    }

    public void SetFullScreen(bool _fullscreen)
    {
        Screen.fullScreen = _fullscreen;
    }

    public void SetQuality(int _qualityIndex)
    {
        QualitySettings.SetQualityLevel(_qualityIndex);
    }

    public void SetResolution(int _resolutionIndedx)
    {
        Resolution res = resolutions[_resolutionIndedx];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }

    private void GetResolutions()
    {
        resolutionDropdown.ClearOptions();
        resolutions = Screen.resolutions;
        resolutionDropdown.AddOptions(ResolutionToString(resolutions));
        resolutionDropdown.value = GetCurrentResolutionIndex(resolutions);
        resolutionDropdown.RefreshShownValue();
    }

    private List<string> ResolutionToString(Resolution[] _resolutions)
    {
        List<string> resolutions = new List<string>();
        foreach (Resolution res in _resolutions)
        {
            resolutions.Add(res.width + "x" + res.height);
        }
        return resolutions;
    }

    private int GetCurrentResolutionIndex(Resolution[] _resolutions)
    {
        int i = 0;

        foreach (Resolution res in _resolutions)
        {
            if(res.width == Screen.currentResolution.width && res.height == Screen.currentResolution.height)
            {
                return i; 
            }
            i++;
        }

        return _resolutions.Length;
    }

    private void SetQualityDropdown()
    {
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        qualityDropdown.RefreshShownValue();
    }

    private void SetFullScreenAtStart()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        Screen.fullScreen = true;
        Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
    }

    private void SetFullscreenToggleAtStart()
    {
        if (Screen.fullScreen.Equals(true))
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
    }

    private void Start()
    {
        SetFullScreenAtStart();
        GetResolutions();
        SetQualityDropdown();
        SetFullscreenToggleAtStart();
    }
}
