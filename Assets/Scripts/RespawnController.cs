﻿using Major.EntitySystem;
using Major.Timer;
using UnityEngine;

namespace Major.GameManagers
{
    public class RespawnController : MonoBehaviour
    {
        private readonly ActionTimer respawnTimer = new ActionTimer(5f);
        private bool isDeadTimerCounting = false;
        private BaseEntity playerEntity;
        private GameObject playerGameObject;
        private Animator playerAnim;

        public delegate void PlayerRespawn();
        public event PlayerRespawn OnPlayerRespawn;

        public static RespawnController Instance { get; set; }

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
        }

        private void Update()
        {
            if (isDeadTimerCounting)
            {
                if (respawnTimer.TimerExpired())
                {
                    SetPlayerActive(playerGameObject);
                    isDeadTimerCounting = false;
                    GlobalDataManager.Instance.UIManager.ScreenDisable(UI.UIScreenType.RESPAWN);
                    OnPlayerRespawn.Invoke();
                }
            }
        }

        public void RespawnPlayer(BaseEntity _entity, GameObject _gameObject)
        {
            playerGameObject = _gameObject;
            isDeadTimerCounting = true;
            playerEntity = _entity;
            respawnTimer.RestartTimer();
            GlobalDataManager.Instance.UIManager.ScreenEnable(UI.UIScreenType.RESPAWN);
        }

        private void ResetEntityStats(BaseEntity _entity)
        {
            _entity.EntityCurrentHealth = _entity.EntityMaxHealth;
            _entity.EntityCurrentArmor = _entity.EntityMaxArmor;
            _entity.IsDead = false;
        }

        private void SetPlayerActive(GameObject _playerGameObject)
        {
            playerAnim = _playerGameObject.GetComponentInChildren<Animator>();
            if (playerAnim != null)
            {
                playerAnim.SetBool("isDead", false);
            }
            ResetEntityStats(playerEntity);
            _playerGameObject.SetActive(true);
        }
    }
}