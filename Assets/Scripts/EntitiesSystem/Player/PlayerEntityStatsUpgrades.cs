﻿using UnityEngine;

namespace Major.EntitySystem
{
    public class PlayerEntityStatsUpgrades : MonoBehaviour
    {
        private readonly int maxHealthUpgrade = 5;
        private readonly int maxArmorUpgrade = 5;
        private readonly int maxMovementUpgrade = 5;
        private readonly int maxDamageUpgrade = 5;

        public int CurrentHealthUpgrade { get; set; } = 0;
        public int CurrentArmorUpgrade { get; set; } = 0;
        public int CurrentMovementUpgrade { get; set; } = 0;
        public int CurrentDamageUpgrade { get; set; } = 0;
        public int MaxHealthUpgrade => maxHealthUpgrade;
        public int MaxArmorUpgrade => maxArmorUpgrade;
        public int MaxMovementUpgrade => maxMovementUpgrade;
        public int MaxDamageUpgrade => maxDamageUpgrade;
    }
}