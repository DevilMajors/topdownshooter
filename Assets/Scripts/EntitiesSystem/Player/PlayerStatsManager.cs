﻿using Major.GameManagers;
using Major.Save;
using System.Collections.Generic;
using UnityEngine;

namespace Major.EntitySystem
{
    public class PlayerStatsManager : MonoBehaviour
    {
        private readonly List<int> upgradeCosts = new List<int> { 200, 400, 600, 800, 1000 };
        private PlayerEntity playerEntity;
        private PlayerEntityStatsUpgrades playerEntityStatsUpgrades;

        public delegate void StatsUpgrade();
        public event StatsUpgrade OnStatsUpgrade;

        private void Start()
        {
            playerEntity = GlobalDataManager.Instance.PlayerEntity;
            playerEntityStatsUpgrades = GlobalDataManager.Instance.PlayerEntityStatsUpgrades;
        }

        private bool SubtractMoney(int _upgradeCost)
        {
            if (playerEntity.MoneyAmount >= _upgradeCost)
            {
                playerEntity.MoneyAmount -= _upgradeCost;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetStatsOnLoad()
        {
            playerEntity.EntityMaxHealth = playerEntity.EntityBaseHealth + (playerEntityStatsUpgrades.CurrentHealthUpgrade * 25);
            playerEntity.EntityMaxArmor = playerEntity.EntityBaseArmor + (playerEntityStatsUpgrades.CurrentArmorUpgrade * 25);
            playerEntity.MaxDamageModifier = playerEntity.EntityBaseDmgModifier + (playerEntityStatsUpgrades.CurrentDamageUpgrade * 25);
            playerEntity.EntityMaxMovementSpeed = playerEntity.EntityBaseMovementSpeed + ((float)playerEntityStatsUpgrades.CurrentMovementUpgrade * 0.25f);
            ResetStats();
        }

        private void SetStatsToBase()
        {
            playerEntityStatsUpgrades.CurrentHealthUpgrade = 0;
            playerEntityStatsUpgrades.CurrentArmorUpgrade = 0;
            playerEntityStatsUpgrades.CurrentMovementUpgrade = 0;
            playerEntityStatsUpgrades.CurrentDamageUpgrade = 0;
            playerEntity.EntityMaxHealth = playerEntity.EntityBaseHealth;
            playerEntity.EntityMaxArmor = playerEntity.EntityBaseArmor;
            playerEntity.MaxDamageModifier = playerEntity.EntityBaseDmgModifier;
            playerEntity.EntityMaxMovementSpeed = playerEntity.EntityBaseMovementSpeed;
            playerEntity.MoneyAmount = playerEntity.BaseMoneyAmount;
            ResetStats();
        }

        private void ResetStats()
        {
            playerEntity.EntityCurrentHealth = playerEntity.EntityMaxHealth;
            playerEntity.EntityCurrentArmor = playerEntity.EntityMaxArmor;
        }

        public void SetPlayerStatsToMaxOnLevelEnter()
        {
            playerEntity.EntityCurrentArmor = playerEntity.EntityMaxArmor;
            playerEntity.EntityCurrentHealth = playerEntity.EntityMaxHealth;
        }

        public void UpgradeHealth()
        {
            if (playerEntityStatsUpgrades.CurrentHealthUpgrade < playerEntityStatsUpgrades.MaxHealthUpgrade)
            {
                if (SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentHealthUpgrade]))
                {
                    playerEntityStatsUpgrades.CurrentHealthUpgrade += 1;
                    playerEntity.EntityMaxHealth += 25;
                    InvokeStatsUpgrade();
                }
            }
        }

        public void UpgradeArmor()
        {
            if (playerEntityStatsUpgrades.CurrentArmorUpgrade < playerEntityStatsUpgrades.MaxArmorUpgrade)
            {
                if (SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentArmorUpgrade]))
                {
                    playerEntityStatsUpgrades.CurrentArmorUpgrade += 1;
                    playerEntity.EntityMaxArmor += 25;
                    InvokeStatsUpgrade();
                }
            }
        }

        public void UpgradeDamage()
        {
            if (playerEntityStatsUpgrades.CurrentDamageUpgrade < playerEntityStatsUpgrades.MaxDamageUpgrade)
            {
                if (SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentDamageUpgrade]))
                {
                    playerEntityStatsUpgrades.CurrentDamageUpgrade += 1;
                    playerEntity.MaxDamageModifier += 0.2f;
                    InvokeStatsUpgrade();
                }
            }
        }

        public void UpgradeMovementSpeed()
        {
            if (playerEntityStatsUpgrades.CurrentMovementUpgrade < playerEntityStatsUpgrades.MaxMovementUpgrade)
            {
                if (SubtractMoney(upgradeCosts[playerEntityStatsUpgrades.CurrentMovementUpgrade]))
                {
                    playerEntityStatsUpgrades.CurrentMovementUpgrade += 1;
                    playerEntity.EntityMaxMovementSpeed += 0.25f;
                    InvokeStatsUpgrade();
                }
            }
        }

        public void OnContinue()
        {
            PlayerData playerData = SaveLoadSystem.LoadPlayer();
            if(playerData != null)
            {
                playerEntityStatsUpgrades.CurrentHealthUpgrade = playerData.PlayerHealthUpgrade;
                playerEntityStatsUpgrades.CurrentArmorUpgrade = playerData.PlayerArmorUpgrade;
                playerEntityStatsUpgrades.CurrentMovementUpgrade = playerData.PlayerMovementSpeedUpgrade;
                playerEntityStatsUpgrades.CurrentDamageUpgrade = playerData.PlayerDamageUpgrade;
                playerEntity.MoneyAmount = playerData.PlayerMoney;
                SetStatsOnLoad();
                InvokeStatsUpgrade();
            }
        }

        public void OnNewGame()
        {
            SetStatsToBase();
            InvokeStatsUpgrade();
        }

        private void InvokeStatsUpgrade()
        {
            if (OnStatsUpgrade != null)
            {
                OnStatsUpgrade.Invoke();
            }
        }
    }
}