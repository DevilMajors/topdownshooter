﻿using UnityEngine;

namespace Major.EntitySystem
{
    public class PlayerEntity : BaseEntity
    {
        [SerializeField] private float maxDamageModifier = 1;
        [SerializeField] private int moneyAmount = 0;

        [SerializeField] private float entityBaseHealth;
        [SerializeField] private float entityBaseArmor;
        [SerializeField] private float entityBaseDmgModifier;
        [SerializeField] private float entityBaseMovementSpeed;
        [SerializeField] private int baseMoneyAmount;

        public float MaxDamageModifier { get => maxDamageModifier; set => maxDamageModifier = value; }
        public int MoneyAmount { get => moneyAmount; set => moneyAmount = value; }
        public float EntityBaseHealth { get => entityBaseHealth; set => entityBaseHealth = value; }
        public float EntityBaseArmor { get => entityBaseArmor; set => entityBaseArmor = value; }
        public float EntityBaseDmgModifier { get => entityBaseDmgModifier; set => entityBaseDmgModifier = value; }
        public float EntityBaseMovementSpeed { get => entityBaseMovementSpeed; set => entityBaseMovementSpeed = value; }
        public int BaseMoneyAmount { get => baseMoneyAmount; set => baseMoneyAmount = value; }
    }
}