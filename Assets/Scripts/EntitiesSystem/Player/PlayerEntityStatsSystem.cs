﻿using Major.GameControllers;
using Major.GameManagers;
using System.Collections;
using UnityEngine;

namespace Major.EntitySystem
{
    public class PlayerEntityStatsSystem : BaseEntityStatsSystem
    {
        [SerializeField] private PlayerEntity playerEntity = null;
        [SerializeField] private Animator playerAnim = null;

        private void Awake()
        {
            playerEntity = GlobalDataManager.Instance.PlayerEntity;
        }

        public void GiveMoney(int _moneyAmount)
        {
            if (playerEntity != null)
            {
                playerEntity.MoneyAmount += _moneyAmount;
                UIStatsRefresh();
            }
        }

        public void DamageEntity(float _dmgAmount, EnemyEntity _enemyEntity)
        {
            if (playerEntity != null)
            {
                base.DamageEntity(playerEntity, _dmgAmount);
                GlobalDataManager.Instance.DifficultyLevelController.RegisterNegativeImpact(_dmgAmount, _enemyEntity);
                UIStatsRefresh();
            }
        }

        public void GiveHealthEntity(int _healAmount)
        {
            if (playerEntity != null)
            {
                base.GiveHealthEntity(playerEntity, _healAmount);
                UIStatsRefresh();
            }
        }

        public void GiveArmorEntity(int _armorAmount)
        {
            if (playerEntity != null)
            {
                base.GiveArmorEntity(playerEntity, _armorAmount);
                UIStatsRefresh();
            }
        }

        public override void Die(BaseEntity _entity)
        {
            _entity.IsDead = true;
            playerAnim.SetBool("isDead", true);
            StartCoroutine("WaitForAnimation", 1.5f);
            RespawnController.Instance.RespawnPlayer(_entity, gameObject);
        }

        private void UIStatsRefresh()
        {
            GlobalDataManager.Instance.UIPlayerSystem.OnPlayerStatsModification();
        }

        private IEnumerator WaitForAnimation(float waitTime)
        {
            yield return new WaitForSeconds(waitTime);
            gameObject.SetActive(false);
        }
    }
}