﻿using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class BaseEntityStatsSystem : MonoBehaviour
    {
        [SerializeField] private Transform healthArmorBar;
        public event Action<float, float> OnHealthArmorEntityChanged;

        public virtual void DamageEntity(BaseEntity _entity, float _dmgAmount)
        {
            if (_entity != null)
            {
                DamageEntityWithArmor(_entity, _dmgAmount);
                PassDataToHealthBar(_entity);
            }
        }

        public virtual void GiveHealthEntity(BaseEntity _entity, int _healAmount)
        {
            if (_entity.EntityCurrentHealth + _healAmount > _entity.EntityMaxHealth)
            {
                _entity.EntityCurrentHealth = _entity.EntityMaxHealth;
            }
            else
            {
                _entity.EntityCurrentHealth += _healAmount;
            }
            PassDataToHealthBar(_entity);
        }

        public virtual void GiveArmorEntity(BaseEntity _entity, int _armorAmount)
        {
            if (_entity.EntityCurrentArmor + _armorAmount > _entity.EntityMaxArmor)
            {
                _entity.EntityCurrentArmor = _entity.EntityMaxArmor;
            }
            else
            {
                _entity.EntityCurrentArmor += _armorAmount;
            }

            PassDataToHealthBar(_entity);
        }

        public virtual void Die(BaseEntity _entity)
        {
            _entity.IsDead = true;
            Destroy(gameObject);
        }

        private void DamageEntityWithArmor(BaseEntity _entity, float _dmgAmount)
        {
            float dmgToArmor = _dmgAmount / 1.5f;

            if (_entity.EntityCurrentArmor > 0)
            {
                float tmpDamage = _entity.EntityCurrentArmor - dmgToArmor;
                _entity.EntityCurrentArmor -= dmgToArmor;
                _entity.EntityCurrentArmor = (float)Math.Round(_entity.EntityCurrentArmor, 1);
                if (_entity.EntityCurrentArmor < 0)
                {
                    _entity.EntityCurrentArmor = 0;
                    _entity.EntityCurrentHealth -= tmpDamage * -1.5f;
                }
            }
            else
            {
                if (_entity.EntityCurrentHealth - _dmgAmount > 0)
                {
                    _entity.EntityCurrentHealth -= _dmgAmount;
                    _entity.EntityCurrentHealth = (float)Math.Round(_entity.EntityCurrentHealth, 1);
                }
                else
                {
                    _entity.EntityCurrentHealth = 0;
                    Die(_entity);
                }
            }
        }

        private void PassDataToHealthBar(BaseEntity _entity)
        {
            if (OnHealthArmorEntityChanged != null)
            {
                float healthPercent = _entity.EntityCurrentHealth / _entity.EntityMaxHealth;
                float armorPercent = _entity.EntityCurrentArmor / _entity.EntityMaxArmor;
                OnHealthArmorEntityChanged(healthPercent, armorPercent);
            }
        }

        public float GetEnemyHealth(BaseEntity _entity)
        {
            return _entity.EntityCurrentHealth;
        }
    }
}