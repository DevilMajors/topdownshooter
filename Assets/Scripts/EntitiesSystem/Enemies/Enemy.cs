﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Major.EntitySystem
{
    public class Enemy : MonoBehaviour
    {
        private Dictionary<Type, BaseState> states = new Dictionary<Type, BaseState>();

        public EnemyEntity EnemyEntity { get; set; }
        public Transform Target { get; private set; }
        public Rigidbody EnemyRigidbody { get; set; }
        public CapsuleCollider EnemyCollider { get; set; }
        public NavMeshAgent NavAgent { get; set; }

        public Transform attackPoint;
        public Animator animator;

        private void Awake()
        {
            NavAgent = GetComponent<NavMeshAgent>();
            EnemyEntity = GetComponent<EnemyEntity>();
            EnemyRigidbody = GetComponent<Rigidbody>();
            EnemyCollider = GetComponent<CapsuleCollider>();
            InitializeStateMachine();
            animator.applyRootMotion = false;
        }

        private void InitializeStateMachine()
        {
            states.Add(typeof(WanderState), new WanderState(this, EnemyEntity.DetectionRange));
            states.Add(typeof(ChaseState), new ChaseState(this, EnemyEntity.StoppingDistance, EnemyEntity.DetectionRange));
            states.Add(typeof(AttackControllState), new AttackControllState(this, EnemyEntity.EnemyType));
            states.Add(typeof(DieState), new DieState(this));
            AddAttackStateBasedOnEnemyType();

            if(states != null)
            {
                GetComponent<EnemiesStateMachine>().SetStates(states);
            }

        }

        private void AddAttackStateBasedOnEnemyType()
        {
            switch (EnemyEntity.EnemyType)
            {
                case EnemyType.EXPLODING:
                    states.Add(typeof(AttackExplodingState), new AttackExplodingState(this));
                    break;
                case EnemyType.MELE:
                    states.Add(typeof(AttackMeleState), new AttackMeleState(this));
                    break;
                case EnemyType.RANGE_PISTOL:
                case EnemyType.RANGE_SHOTGUN:
                case EnemyType.RANGE_RIFLE:
                    states.Add(typeof(AttackRangeState), new AttackRangeState(this));
                    break;
                case EnemyType.CHARGER:
                    states.Add(typeof(AttackChargeState), new AttackChargeState(this));
                    break;
            }
        }

        public void SetTarget(Transform _target)
        {
            Target = _target;
        }

/*        var states = new Dictionary<Type, BaseState>()
        {
            { typeof(WanderState), new WanderState(this, EnemyEntity.DetectionRange) },
            { typeof(ChaseState), new ChaseState(this, EnemyEntity.StoppingDistance, EnemyEntity.DetectionRange) },
            { typeof(AttackControllState), new AttackControllState(this, EnemyEntity.EnemyType)},

            { typeof(AttackExplodingState), new AttackExplodingState(this) },
            { typeof(AttackRangeState), new AttackRangeState(this) },
            { typeof(AttackChargeState), new AttackChargeState(this) },
            { typeof(DieState), new DieState(this) }
        };*/
    }
}