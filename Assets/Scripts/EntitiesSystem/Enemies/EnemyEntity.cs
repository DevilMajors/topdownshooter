﻿using System;
using UnityEngine;

namespace Major.EntitySystem
{
    [Serializable]
    public class EnemyEntity : BaseEntity
    {
        [SerializeField] private EnemyType enemyType;
        [SerializeField] private float detectionRange;
        [SerializeField] private float attackRange;
        [SerializeField] private float stoppingDistance;
        [SerializeField] private float attackDelay;
        [SerializeField] private float attackDamage;
        [SerializeField] private int pointsAmount;

        public float AttackRange { get => attackRange; set => attackRange = value; }
        public float StoppingDistance { get => stoppingDistance; set => stoppingDistance = value; }
        public float DetectionRange { get => detectionRange; set => detectionRange = value; }
        public EnemyType EnemyType { get => enemyType; set => enemyType = value; }
        public float AttackDelay { get => attackDelay; set => attackDelay = value; }
        public float AttackDamage { get => attackDamage; set => attackDamage = value; }
        public bool ReceivedDamage { get; set; } = false;
        public int PointsAmount { get => pointsAmount; set => pointsAmount = value; }
    }

    public enum EnemyType
    {
        EXPLODING,
        MELE,
        RANGE_PISTOL,
        RANGE_RIFLE,
        RANGE_SHOTGUN,
        SUPPORT,
        CHARGER,
        NULL
    }
}