﻿using Major.Timer;
using System;
using UnityEngine;
using UnityEngine.AI;

namespace Major.EntitySystem
{
    public class WanderState : BaseState
    {
        private readonly float detectionRange;
        private readonly Enemy enemy;
        private Vector3 destinationPoint;
        private bool isWalking = false;

        private readonly ActionTimer IdleTimer = new ActionTimer(5f);

        public WanderState(Enemy _enemy, float _detectionDistance) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            detectionRange = _detectionDistance;
            destinationPoint = this.transform.position;
        }

        public override Type Tick()
        {
            Transform chaseTarget = CheckForPlayerEntity();
            if (chaseTarget != null)
            {
                enemy.SetTarget(chaseTarget);
                return typeof(ChaseState);
            }

            if (ReachedTarget())
            {
                isWalking = false;
                enemy.NavAgent.enabled = false;
                enemy.NavAgent.enabled = true;
                enemy.animator.SetBool("isWalking", false);
                SetNewDestination();
            }
            
            if(!isWalking)
            {
                if (IdleTimer.TimerCycle())
                {
                    isWalking = true;
                }
            }
            else
            {
                enemy.animator.SetBool("isWalking", true);
                WanderTowardsDestinationPoint();
            }
            return typeof(WanderState);
        }

        private Transform CheckForPlayerEntity()
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, detectionRange);
            Transform target = null;
            if (cols != null)
            {
                foreach (Collider col in cols)
                {
                    if (col.GetComponent<PlayerEntityStatsSystem>() != null)
                    {
                        target = col.transform;
                    }
                }
            }
            return target;
        }

        public Vector3 RandomNavmeshLocation(float radius)
        {
            Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
            randomDirection += transform.position;
            NavMeshHit hit;
            Vector3 finalPosition = Vector3.zero;
            if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
            {
                finalPosition = hit.position;
            }
            return finalPosition;
        }

        private void SetNewDestination()
        {
            destinationPoint = RandomNavmeshLocation(detectionRange);
        }

        private void WanderTowardsDestinationPoint()
        {
            enemy.NavAgent.SetDestination(destinationPoint);
        }

        private bool ReachedTarget()
        {
            float distanceToDestination = Vector3.Distance(transform.position, destinationPoint);
            return distanceToDestination <= enemy.NavAgent.stoppingDistance;
        }
    }
}