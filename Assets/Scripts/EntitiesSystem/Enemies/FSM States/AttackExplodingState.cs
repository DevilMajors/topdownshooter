﻿using Major.Timer;
using System;

namespace Major.EntitySystem
{
    public class AttackExplodingState : BaseState
    {
        private readonly ActionTimer attackDelay;

        public AttackExplodingState(Enemy _enemy) : base(_enemy.gameObject)
        {
            attackDelay = new ActionTimer(_enemy.EnemyEntity.AttackDelay);
        }

        public override Type Tick()
        {
            if (attackDelay.TimerCycle())
            {
                return typeof(DieState);
            }
            return typeof(AttackExplodingState);
        }
    }
}