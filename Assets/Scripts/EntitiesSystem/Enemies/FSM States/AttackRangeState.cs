﻿using Major.Timer;
using Major.WeaponSystem;
using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class AttackRangeState : BaseState
    {
        private readonly Enemy enemy;
        private readonly EnemyWeaponController enemyWeaponController;
        private readonly ActionTimer attackDelay;
        public AttackRangeState(Enemy _enemy) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            enemyWeaponController = enemy.gameObject.GetComponent<EnemyWeaponController>();
            attackDelay = new ActionTimer(enemy.EnemyEntity.AttackDelay);
        }

        public override Type Tick()
        {
            FacePlayer();
            enemy.EnemyRigidbody.velocity = Vector3.zero;
            if (attackDelay.TimerCycle())
            {
                enemy.animator.SetBool("isAttacking", true);
                enemyWeaponController.EnemyWeapon.ShootWeapon();
            }
            return typeof(AttackControllState);
        }

        private void FacePlayer()
        {
            var rotation = enemy.Target.position - transform.position;
            rotation.y = 0;
            transform.rotation = Quaternion.LookRotation(rotation);
        }
    }
}