﻿using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class ChaseState : BaseState
    {
        private readonly float stoppingDistance;
        private readonly Enemy enemy;

        public ChaseState(Enemy _enemy, float _stopDistance, float _detectionRange) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            stoppingDistance = _stopDistance;
        }

        public override Type Tick()
        {
            if (enemy.Target != null && !IsPlayerDead())
            {
                if (ChaseToStopDistance())
                {
                    enemy.animator.SetBool("isWalking", false);
                    return typeof(AttackControllState);
                }
                else
                {
                    enemy.animator.SetBool("isWalking", true);
                    enemy.SetTarget(null);
                    return typeof(WanderState);
                }
            }
            return typeof(WanderState);
        }

        private bool ChaseToStopDistance()
        {
            if (Vector3.Distance(transform.position, enemy.Target.position) <= stoppingDistance)
            {
                enemy.NavAgent.isStopped = true;
                return true;
            }
            else
            {
                enemy.NavAgent.isStopped = false;
                enemy.NavAgent.destination = enemy.Target.position;
                return false;
            }
        }
    }
}