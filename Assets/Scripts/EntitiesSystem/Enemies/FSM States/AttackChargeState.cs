﻿using Major.Timer;
using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class AttackChargeState : BaseState
    {
        private readonly Enemy enemy;
        private readonly ActionTimer chargeTime;
        private readonly ActionTimer attackDelay;

        private bool isCharging = false;
        
        public AttackChargeState(Enemy _enemy) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            chargeTime = new ActionTimer(enemy.EnemyEntity.AttackDelay);
            attackDelay = new ActionTimer(enemy.EnemyEntity.AttackDelay);
        }

        public override Type Tick()
        {
            if (!isCharging && attackDelay.TimerCycle())
            {
                FacePlayer();
                isCharging = true;
                
            }
            else if (isCharging)
            {
                if (!chargeTime.TimerCycle())
                {
                    ChargeAtPlayer();
                    enemy.animator.SetBool("isPreparing", false);
                    enemy.animator.SetBool("isAttacking", true);
                }
                else
                {
                    isCharging = false;
                    enemy.animator.SetBool("isAttacking", false);
                    enemy.EnemyRigidbody.velocity = Vector3.zero;
                    return typeof(AttackControllState);
                }
            }
            else
            {
                FacePlayer();
                enemy.animator.SetBool("isPreparing", true);
            }
            return typeof(AttackChargeState);
        }

        private void ChargeAtPlayer()
        {
            enemy.EnemyRigidbody.velocity = enemy.transform.forward * enemy.EnemyEntity.EntityMaxMovementSpeed * 2;
        }

        private void FacePlayer()
        {
            var rotation = enemy.Target.position - transform.position;
            rotation.y = 0;
            transform.rotation = Quaternion.LookRotation(rotation);
        }
    }
}