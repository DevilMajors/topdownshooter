﻿using Major.GameManagers;
using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public abstract class BaseState
    {
        protected GameObject gameObject;
        protected Transform transform;
        public abstract Type Tick();

        protected BaseState(GameObject gameObject)
        {
            this.gameObject = gameObject;
            transform = gameObject.transform;
        }

        protected bool IsPlayerDead()
        {
            return GlobalDataManager.Instance.PlayerEntity.IsDead;
        }
    }
}