﻿using Major.Timer;
using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class AttackMeleState : BaseState
    {
        private readonly Enemy enemy;
        private readonly ActionTimer attackDelay;

        public AttackMeleState(Enemy _enemy) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            attackDelay = new ActionTimer(enemy.EnemyEntity.AttackDelay);
        }

        public override Type Tick()
        {
            enemy.animator.SetBool("isAttacking", true);
            FacePlayer();
            enemy.EnemyRigidbody.velocity = Vector3.zero;
            if (attackDelay.TimerCycle())
            {
                enemy.animator.SetBool("isAttacking", true);
                MeleAttackPlayer();
            }
            return typeof(AttackControllState);
        }

        private void MeleAttackPlayer()
        {
            GetAndDamagePlayer(enemy.attackPoint);
        }

        private void FacePlayer()
        {
            var rotation = enemy.Target.position - transform.position;
            rotation.y = 0;
            transform.rotation = Quaternion.LookRotation(rotation);
        }

        private void GetAndDamagePlayer(Transform _attackPoint)
        {
            Collider[] cols = Physics.OverlapSphere(_attackPoint.position, enemy.EnemyEntity.AttackRange, -1);
            foreach (var collider in cols)
            {
                if (collider.GetComponent<PlayerEntityStatsSystem>() != null)
                {
                    collider.GetComponent<PlayerEntityStatsSystem>().DamageEntity(enemy.EnemyEntity.AttackDamage, enemy.EnemyEntity);
                }
            }
        }
    }
}