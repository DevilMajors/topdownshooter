﻿using Major.GameManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Major.EntitySystem
{
    public class EnemiesStateMachine : MonoBehaviour
    {
        private Dictionary<Type, BaseState> _availableStates;

        public BaseState CurrentState { get; set; }
        public event Action<BaseState> OnStateChanged;

        public void SetStates(Dictionary<Type, BaseState> states)
        {
            _availableStates = states;
        }

        public void SwitchToNewState(Type _nextState)
        {
            if (_availableStates.ContainsKey(_nextState))
            {
                CurrentState = _availableStates[_nextState];
                OnStateChanged?.Invoke(CurrentState);
            }
            else
            {
                CurrentState = _availableStates.Values.First();
            }

        }

        private void FixedUpdate()
        {
            if (!ManagerGame.Instance.gameIsPaused)
            {
                if (CurrentState == null)
                {
                    CurrentState = _availableStates.Values.First();
                }

                var nextState = CurrentState?.Tick();

                if (nextState != null && nextState != CurrentState?.GetType())
                {
                    SwitchToNewState(nextState);
                }
            }
        }
    }
}