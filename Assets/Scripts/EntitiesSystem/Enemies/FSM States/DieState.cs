﻿using Major.GameManagers;
using Major.GameManagers.VFXManager;
using Major.Timer;
using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class DieState : BaseState
    {
        private readonly Enemy enemy;
        private readonly EnemyEntityStatsSystem entityStatsSystem;
        private readonly EnemyType enemyType;
        private readonly ActionTimer delayTimer;
        private bool pointsGiven = false;

        public DieState(Enemy _enemy) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            entityStatsSystem = enemy.gameObject.GetComponent<EnemyEntityStatsSystem>();
            enemyType = _enemy.EnemyEntity.EnemyType;
            delayTimer = new ActionTimer(enemy.EnemyEntity.AttackDelay);
        }

        public override Type Tick()
        {
            if (entityStatsSystem != null)
            {
                enemy.EnemyRigidbody.velocity = Vector3.zero;
                enemy.NavAgent.speed = 0;
                switch (enemyType)
                {
                    case EnemyType.EXPLODING:
                        if (delayTimer.TimerCycle())
                        {
                            ExplodeOnDeath(enemy.EnemyEntity.AttackDamage);
                            entityStatsSystem.DestroyGameObject(gameObject, 0f);
                        }
                        break;
                    default:
                        GivePointsAndCount();
                        enemy.animator.SetBool("isDead", true);
                        entityStatsSystem.DestroyGameObject(gameObject, 1.5f);
                        break;
                }
            }
            return null;
        }

        private void ExplodeOnDeath(float _dmgToDeal)
        {
            Collider[] enemiesToDamage = GetEnemiesInExplosionRange(3f);
            VfxManager.Instance.PlayVFX("EXPLOSION", 2f, transform);
            if (enemiesToDamage.Length > 0)
            {
                foreach (Collider col in enemiesToDamage)
                {
                    GameObject tempGameObject = col.gameObject;
                    if (tempGameObject.GetComponent<PlayerEntityStatsSystem>() != null)
                    {
                        tempGameObject.GetComponent<PlayerEntityStatsSystem>().DamageEntity(_dmgToDeal, enemy.EnemyEntity);
                    }
                    else if (tempGameObject.GetComponent<EnemyEntityStatsSystem>() != null)
                    {
                        tempGameObject.GetComponent<EnemyEntityStatsSystem>().DamageEntity(_dmgToDeal);
                    }
                }
            }
            GivePointsAndCount();
        }
        private Collider[] GetEnemiesInExplosionRange(float _damageRange)
        {
            return Physics.OverlapSphere(transform.position, _damageRange);
        }

        private void GivePointsAndCount()
        {
            if (!pointsGiven)
            {
                GlobalDataManager.Instance.PointsSystem.CountPoints(enemy.EnemyEntity.PointsAmount);
                pointsGiven = true;
                if (GlobalDataManager.Instance.EnemyCounter != null)
                {
                    GlobalDataManager.Instance.EnemyCounter.EnemyDeath();
                }
            }
        }
    }
}