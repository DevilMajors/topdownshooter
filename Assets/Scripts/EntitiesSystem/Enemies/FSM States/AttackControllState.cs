﻿using System;
using UnityEngine;

namespace Major.EntitySystem
{
    public class AttackControllState : BaseState
    {
        private readonly EnemyType enemyType;
        private readonly Enemy enemy;

        public AttackControllState(Enemy _enemy, EnemyType _enemyType) : base(_enemy.gameObject)
        {
            enemy = _enemy;
            enemyType = _enemyType;
        }

        public override Type Tick()
        {
            if (!IsPlayerDead())
            {
                if (!PlayerInAttackRange() && enemy.animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
                {
                    enemy.animator.SetBool("isAttacking", false);
                    return typeof(ChaseState);
                }
                else
                {
                    switch (enemyType)
                    {
                        case EnemyType.EXPLODING:
                            return typeof(DieState);
                        case EnemyType.MELE:
                            return typeof(AttackMeleState);
                        case EnemyType.RANGE_PISTOL:
                        case EnemyType.RANGE_SHOTGUN:
                        case EnemyType.RANGE_RIFLE:
                            return typeof(AttackRangeState);
                        case EnemyType.SUPPORT:
                            return typeof(AttackMeleState);
                        case EnemyType.CHARGER:
                            return typeof(AttackChargeState);
                    }
                }
            }
            else
            {
                enemy.animator.SetBool("isAttacking", false);
                return typeof(WanderState);
            }
            return null;
        }

        private bool PlayerInAttackRange()
        {
            return Vector3.Distance(transform.position, enemy.Target.position) <= enemy.EnemyEntity.AttackRange;
        }
    }
}