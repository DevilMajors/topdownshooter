﻿using UnityEngine;
using Major.WeaponSystem;

namespace Major.EntitySystem
{
    public class EnemyEntityStatsSystem : BaseEntityStatsSystem
    {
        [SerializeField] private EnemyEntity enemyEntity = null;
        private Enemy enemy;
        private DropOnDeath dropOnDeath;
        private bool itemDroped = false;

        private void Awake()
        {
            enemy = GetComponent<Enemy>();
            dropOnDeath = GetComponent<DropOnDeath>();
        }

        public EnemyEntity EnemyEntity { get => enemyEntity; private set => enemyEntity = value; }

        public void DamageEntity(float _dmgAmount)
        {
            base.DamageEntity(EnemyEntity, _dmgAmount);
            enemyEntity.ReceivedDamage = true;
        }

        public void GiveHealthEntity(int _healAmount)
        {
            base.GiveHealthEntity(EnemyEntity, _healAmount);
        }

        public void GiveArmorEntity(int _armorAmount)
        {
            base.GiveArmorEntity(EnemyEntity, _armorAmount);
        }

        public override void Die(BaseEntity _entity)
        {
            EnemiesStateMachine enemiesStateMachine = GetComponent<EnemiesStateMachine>();
            if (enemiesStateMachine != null)
            {
                enemiesStateMachine.SwitchToNewState(typeof(DieState));
            }
        }

        public void DestroyGameObject(GameObject _gameObject, float _time)
        {
            if(gameObject.GetComponent<EnemyWeaponController>() != null)
            {
                gameObject.GetComponent<EnemyWeaponController>().EnemyWeapon.gameObject.SetActive(false);
            }

            if(dropOnDeath != null && !itemDroped)
            {
                dropOnDeath.DropItem();
                itemDroped = true;
            }
            Destroy(_gameObject, _time);
        }
    }
}