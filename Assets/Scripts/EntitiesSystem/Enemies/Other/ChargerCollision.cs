﻿using UnityEngine;

namespace Major.EntitySystem
{
    public class ChargerCollision : MonoBehaviour
    {
        private EnemyEntity enemyEntity;

        private void Awake()
        {
            enemyEntity = transform.parent.gameObject.GetComponent<EnemyEntity>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && enemyEntity != null && other.GetComponent<PlayerEntityStatsSystem>() != null)
            {
                other.GetComponent<PlayerEntityStatsSystem>().DamageEntity(enemyEntity.AttackDamage, enemyEntity);
            }
        }
    }
}