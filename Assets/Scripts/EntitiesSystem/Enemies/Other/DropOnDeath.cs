﻿using UnityEngine;

namespace Major.EntitySystem
{
    public class DropOnDeath : MonoBehaviour
    {
        [SerializeField] private DropListScriptable dropList = null;

        public void DropItem()
        {
            int dropChance = Random.Range(1, 10);
            int dropAmount = GetDropAmount(dropChance);

            for (int i = 0; i < dropAmount; i++)
            {
                Vector3 tmpDropPlace = this.transform.position;
                tmpDropPlace.y = RaycastToGround().position.y;
                Instantiate(ItemToDrop(), tmpDropPlace, Quaternion.identity);
            }
        }

        private GameObject ItemToDrop()
        {
            int dropRate = Random.Range(0, 20);
            if(dropRate < 10)
            {
                return dropList.DropList[0];
                
            }else if(dropRate >= 10 && dropRate < 15)
            {
                return dropList.DropList[Random.Range(1, 3)];
            }
            else if(dropRate >= 15 && dropRate <= 18)
            {
                return dropList.DropList[Random.Range(3, 5)];
            }
            else
            {
                return dropList.DropList[Random.Range(5, dropList.DropList.Count)];
            }
        }
        
        private int GetDropAmount(int _chance)
        {
            if(_chance <= 5)
            {
                return 0;
            }else if(_chance >= 6 && _chance < 9)
            {
                return 1;
            }
            else{
                return 2;
            }
        }

        private Transform RaycastToGround()
        {
            RaycastHit hit;
            Ray downRay = new Ray(transform.position, -Vector3.up);
            Physics.Raycast(downRay, out hit);

            return hit.transform;
        }
    }
}