﻿using System.Collections.Generic;
using UnityEngine;

namespace Major.EntitySystem
{
    [CreateAssetMenu(fileName = "DropList", menuName = "ScriptableObjects/DropList", order = 1)]
    public class DropListScriptable : ScriptableObject
    {
        [SerializeField] private List<GameObject> dropList;

        public List<GameObject> DropList { get => dropList; set => dropList = value; }
    }
}