﻿using UnityEngine;

namespace Major.EntitySystem
{
    public class BaseEntity : MonoBehaviour
    {
        [SerializeField] private float entityCurrentHealth;
        [SerializeField] private float entityMaxHealth;
        [SerializeField] private float entityCurrentArmor;
        [SerializeField] private float entityMaxArmor;
        [SerializeField] private float entityMaxMovementSpeed;
        [SerializeField] private bool isDead;

        public float EntityMaxMovementSpeed { get => entityMaxMovementSpeed; set => entityMaxMovementSpeed = value; }
        public float EntityCurrentHealth { get => entityCurrentHealth; set => entityCurrentHealth = value; }
        public float EntityMaxHealth { get => entityMaxHealth; set => entityMaxHealth = value; }
        public float EntityCurrentArmor { get => entityCurrentArmor; set => entityCurrentArmor = value; }
        public float EntityMaxArmor { get => entityMaxArmor; set => entityMaxArmor = value; }
        public bool IsDead { get => isDead; set => isDead = value; }
    }
}