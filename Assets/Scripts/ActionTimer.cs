﻿using UnityEngine;

namespace Major.Timer
{
    public class ActionTimer
    {
        private readonly float timeToWait;
        private float timer;
        private bool isPaused;

        public ActionTimer(float timeToWait)
        {
            this.timeToWait = timeToWait;
        }

        private void CountTime()
        {
            if (!isPaused)
            {
                timer += Time.deltaTime;
            }
        }

        public bool TimerCycle()
        {
            CountTime();
            if (timer >= timeToWait)
            {
                RestartTimer();
                return true;
            }
            return false;
        }

        public bool TimerExpired()
        {
            CountTime();
            if (timer >= timeToWait)
            {
                return true;
            }
            return false;
        }

        public void StartTimer()
        {
            isPaused = false;
        }

        public void StopTimer()
        {
            isPaused = true;
        }

        public void RestartTimer()
        {
            timer = 0;
        }
    }
}