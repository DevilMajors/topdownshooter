﻿using Major.GameControllers;
using Major.Level.Triggers;
using Major.Save;
using Major.UI;
using Major.WeaponSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Major.GameManagers
{
    public class ManagerGame : MonoBehaviour
    {
        public GameObject loadingScreen;
        public UnityEvent OnScreenChange;
        public bool gameIsPaused = false;

        private readonly List<AsyncOperation> sceneLoading = new List<AsyncOperation>();
        private UIStageInfo uiStageInfo;

        private int currentScene;
        private int previousScene;

        public delegate void StageEnter();
        public event StageEnter OnStageEntered;
        public event StageEnter OnSceneLoaded;

        public delegate void LoadGame();
        public event LoadGame OnGameLoad;

        public delegate void NewGame();
        public event NewGame OnNewGame;

        public int CurrentScene { get => currentScene; private set => currentScene = value; }
        public int PreviousScene { get => previousScene; private set => previousScene = value; }
        public static ManagerGame Instance { get; set; }

        private void Awake()
        {
            if (Instance == null || Instance != this)
            {
                Instance = this;
            }
            InitializeScene();
        }

        private void InitializeScene()
        {
            if (uiStageInfo == null)
            {
                uiStageInfo = GetComponent<UIStageInfo>();
            }
            loadingScreen.SetActive(false);
            CurrentScene = SceneManager.GetActiveScene().buildIndex;

            LoadSceneByNumber(1);
        }

        private bool SceneExists(int _sceneNumber)
        {
            return Application.CanStreamedLevelBeLoaded(_sceneNumber);
        }

        private bool SceneIsLoaded(int _sceneNumber)
        {
            return SceneManager.GetSceneByBuildIndex(_sceneNumber).isLoaded;
        }

        public void LoadSceneByNumber(int _sceneNumber)
        {
            if (SceneExists(_sceneNumber) && loadingScreen != null)
            {
                loadingScreen.SetActive(true);

                if (SceneIsLoaded(CurrentScene))
                {
                    sceneLoading.Add(SceneManager.UnloadSceneAsync(CurrentScene));
                }
                sceneLoading.Add(SceneManager.LoadSceneAsync(_sceneNumber, LoadSceneMode.Additive));
                PreviousScene = CurrentScene;
                CurrentScene = _sceneNumber;

                StartCoroutine(GetLoadProgress());
            }

            if (_sceneNumber > 1 && OnStageEntered != null)
            {
                OnStageEntered.Invoke();
            }
        }

        public void StageCompleteTrigger()
        {
            uiStageInfo.MarkStageAsCompleted(currentScene);
            SaveLoadSystem.SavePlayer();
            SaveLoadSystem.SaveStageInfo();
            LoadSceneByNumber(2);
        }

        public void OnStageExit()
        {
            foreach (var item in FindObjectsOfType<PickUp>())
            {
                Destroy(item.gameObject);
            }

            foreach (var item in FindObjectsOfType<Bullet>())
            {
                GameObjectPool.Instance.ReturnToPool(item.gameObject);
            }
        }
        public void PauseGame()
        {
            gameIsPaused = true;
            Time.timeScale = 0f;
        }

        public void UnpauseGame()
        {
            gameIsPaused = false;
            Time.timeScale = 1f;
        }

        public void SetStatsOnNewGame()
        {
            OnNewGame?.Invoke();
        }

        public void SetStatsOnContinue()
        {
            OnGameLoad?.Invoke();
        }

        public void QuitApplication()
        {
            Application.Quit();
        }

        public IEnumerator GetLoadProgress()
        {
            foreach (AsyncOperation op in sceneLoading)
            {
                while (!op.isDone)
                {
                    yield return null;
                }
            }

            loadingScreen.SetActive(false);
            OnScreenChange?.Invoke();
            OnSceneLoaded?.Invoke();
        }
    }
}